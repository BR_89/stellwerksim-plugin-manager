package de.sts_plugins.api.http;

/**
 *
 * @author BR 89
 */
public interface IEndpoint {
    boolean handles(IRequest request);
    void handle(IRequest request, IResponse response);
}
