package de.sts_plugins.api.http;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BR 89
 */
public class EndpointRegistry {
    private static EndpointRegistry instance;
    private final List<IEndpoint> endpoints = new LinkedList<>();
    
    private EndpointRegistry() {
    }
    
    public void addEndpoint(IEndpoint endpoint) {
        endpoints.add(endpoint);
    }
    
    public boolean isRequestHandled(IRequest request) {
        return endpoints.stream().anyMatch((endpoint) -> (endpoint.handles(request)));
    }
    
    public IEndpoint getEndpointForRequest(IRequest request) {
        for (IEndpoint endpoint : endpoints) {
            if (endpoint.handles(request)) {
                return endpoint;
            }
        }
        return null;
    }

    public static EndpointRegistry getInstance() {
        if (instance == null) {
            instance = new EndpointRegistry();
        }
        return instance;
    }
}
