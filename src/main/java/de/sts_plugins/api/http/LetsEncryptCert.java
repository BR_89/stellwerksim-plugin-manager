package de.sts_plugins.api.http;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author BR 89
 */
public class LetsEncryptCert {

    public static void addLetsEncyptRootCA() {
        try (InputStream fis = new BufferedInputStream(LetsEncryptCert.class.getResourceAsStream("/DSTRootCAX3.pem"))) {
            Certificate ca = CertificateFactory.getInstance("X.509").generateCertificate(fis);
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            ks.setCertificateEntry(Integer.toString(1), ca);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, tmf.getTrustManagers(), null);
            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
            Logger.getLogger(LetsEncryptCert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
