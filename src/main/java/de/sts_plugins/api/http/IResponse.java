package de.sts_plugins.api.http;

/**
 *
 * @author BR 89
 */
public interface IResponse {

    void setCode(int code);

    void setContentType(String contentType);
    
    void serializeToJson(Object object, Class clazz);
    
}
