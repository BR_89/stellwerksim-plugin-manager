package de.sts_plugins.api.plugin;

import java.util.List;

/**
 *
 * @author BR 89
 */
public interface IManagedObjectFilter {

    List<IManagedObject> getMatchingElements(List<IManagedObject> input);
    
}
