package de.sts_plugins.api.plugin.starter;

import de.sts_plugins.api.utils.UUIDObjectMapper;
import de.sts_plugins.api.utils.filesystem.UUIDFilesystemMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author BR 89
 */
public class JarStarter implements IStarter, Runnable {

    protected UUID uuid;
    protected String jar;
    protected String classpath;
    protected String main;
    
    @Override
    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }
    
    @Override
    public void setParameter(String name, String value) {
        switch(name) {
            case "jar":
                this.jar = value;
                break;
            case "classpath":
                this.classpath = value;
                break;
            case "main":
                this.main = value;
                break;
            default:
        }
    }
    
    @Override
    public void run() {
        List<String> arguments = new ArrayList<>();

        String path = System.getProperty("java.home") + File.separator + "bin"
                + File.separator + "java";
        arguments.add(path);
        arguments.add("-cp");
        arguments.add(UUIDFilesystemMapper.getInstance().getPath(uuid) + File.separator
                + jar + File.pathSeparator + classpath);
        arguments.add(main);

        Logger.getLogger(JarStarter.class.getName()).log(Level.INFO, "Would start with: {0}", arguments);
        ProcessBuilder processBuilder = new ProcessBuilder(new String[0]);
        processBuilder.command(arguments);
        try {
            Process process = processBuilder.start();
            try {
                if (process.waitFor() != 0) {
                    JOptionPane.showMessageDialog(null, "Läuft der Sim?",
                            UUIDObjectMapper.getInstance().get(uuid).getName() + " Fehler!", JOptionPane.ERROR_MESSAGE);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(JarStarter.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(JarStarter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
