package de.sts_plugins.api.plugin.starter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author BR 89
 */
public class StarterRegistry {
    private static StarterRegistry instance;
    private final Map<String, IStarter> map = new HashMap<>();

    public static StarterRegistry getInstance() {
        if (instance == null) {
            instance = new StarterRegistry();
        }
        return instance;
    }
}
