package de.sts_plugins.api.plugin.starter;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public interface IStarter {
    void setUUID(UUID uuid);
    void setParameter(String name, String value);
}
