package de.sts_plugins.api.plugin;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public final class Requirement {

    protected UUID uuid;
    protected String version;

    public Requirement(UUID uuid, String version) {
        this.uuid = uuid;
        this.version = version;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getVersion() {
        return version;
    }

}
