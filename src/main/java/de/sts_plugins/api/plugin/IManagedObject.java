package de.sts_plugins.api.plugin;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public interface IManagedObject {
    UUID getUUID();
    String getName();
    String getVersion();
    Requirement[] getRequirements();
}
