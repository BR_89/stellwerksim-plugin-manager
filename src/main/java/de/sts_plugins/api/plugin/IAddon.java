package de.sts_plugins.api.plugin;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public interface IAddon {
    UUID getParentUUID();
}
