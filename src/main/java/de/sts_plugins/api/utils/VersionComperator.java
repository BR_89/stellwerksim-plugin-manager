package de.sts_plugins.api.utils;

/**
 *
 * @author BR 89
 */
public class VersionComperator {

    public static final String INTERVALL_SEPERATOR = ",";
    public static final String VERSION_SEPERATOR = ";";
    
    public static int compareVersionStrings(final String a, final String b) {
        if (a == null && b == null) {
            return 0;
        }
        if (a == null) {
            return -1;
        }
        if (b == null) {
            return 1;
        }
        final String[] aParts = a.split("\\.");
        final String[] bParts = b.split("\\.");
        int j = Math.min(aParts.length, bParts.length);
        int i = 0;
        while (i < j) {
            if (!aParts[i].equals(bParts[i])) {
                String aPart = aParts[i];
                String aPartFollow = null;
                String bPart = bParts[i];
                String bPartFollow = null;
                if (aParts[i].contains("-")) {
                    aPart = aParts[i].split("-")[0];
                    aPartFollow = aParts[i].split("-")[1];
                }
                if (bParts[i].contains("-")) {
                    bPart = bParts[i].split("-")[0];
                    bPartFollow = bParts[i].split("-")[1];
                }
                if (!aPart.equals(bPart)) {
                    return Integer.signum(Integer.compare(
                            Integer.parseInt(aPart),
                            Integer.parseInt(bPart)));
                } else {
                    if (aPartFollow != null && bPartFollow != null) {
                        return Integer.signum(aPartFollow.compareTo(bPartFollow));
                    } else if (aPartFollow != null) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
            i++;
        }
        if (i < aParts.length) {
            return 1;
        } else if (i < bParts.length) {
            return -1;
        }
        return 0;
    }

    public static boolean versionInIntervalls(final String version, final String intervalls) {
        boolean foundIntervallSeperator = intervalls.contains(INTERVALL_SEPERATOR);
        boolean foundVersionSeperator = intervalls.contains(VERSION_SEPERATOR);
        if (!foundIntervallSeperator && !foundVersionSeperator) {
            return version.equals(intervalls);
        }
        for (String intervall : splitInvervall(intervalls)) {
            if (versionInIntervall(version, intervall))
            {
                return true;
            }
        }
        return false;
    }
    
    protected static String[] splitInvervall(final String intervalls) {
        return intervalls.split(INTERVALL_SEPERATOR);
    }

    protected static boolean versionInIntervall(final String version, final String intervall) {
        String[] versions = intervall.split(VERSION_SEPERATOR);
        int left;
        int right;
        int leftMargin = 0;
        int rightMargin = 0;
        if (versions[0].charAt(0) == '(') {
            leftMargin = 1;
        }
        if (versions[1].charAt(versions[1].length()-1) == ')') {
            rightMargin = -1;
        }
        left = compareVersionStrings(version, versions[0].substring(1));
        right = compareVersionStrings(version, versions[1].substring(0, versions[1].length()-1));
        return (left >= leftMargin && right <= rightMargin);
    }
}
