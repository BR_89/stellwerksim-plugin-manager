package de.sts_plugins.api.utils;

import de.sts_plugins.api.utils.filesystem.FilesystemHelper;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class Updater {

    private URL source;
    private Class main;
    private final HashMap<HashChecker.Type, String> hashmap = new HashMap<>();
    private String[] args;
    private File runningJar;
    private File temporaryStorage;

    public Updater() {
    }

    public void setSource(URL source) {
        this.source = source;
    }

    public void addHash(HashChecker.Type type, String hash) {
        hashmap.put(type, hash);
    }

    public void setMain(Class main) {
        this.main = main;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public void run() {
        if (download()) {
            startUpdateProcess();
        }
    }

    protected boolean checkFile(File file) {
        boolean result = true;
        for (Map.Entry<HashChecker.Type, String> entry : hashmap.entrySet()) {
            HashChecker.Type type = entry.getKey();
            String hash = entry.getValue();
            try {
                result &= hash.equals(HashChecker.calculateFileHash(file, type));
            } catch (NoSuchAlgorithmException | IOException ex) {
                Logger.getLogger(Updater.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return result;
    }

    protected boolean download() {
        String filename = source.toString().substring(source.toString().lastIndexOf("/"));
        temporaryStorage = new File(System.getProperty("java.io.tmpdir") + File.separator + filename);
        if (!temporaryStorage.exists() || !checkFile(temporaryStorage)) {
            Downloader.downloadToFile(source, temporaryStorage);
        }
        return checkFile(temporaryStorage);
    }

    protected void startUpdateProcess() {
        if (runningJar == null) {
            runningJar = new File(main.getProtectionDomain().getCodeSource().getLocation().getPath());
        }
        List<String> command = new LinkedList<>();

        String path = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        command.add(path);
        command.add("-cp");
        command.add(temporaryStorage.getAbsolutePath());
        command.add(Updater.class.getCanonicalName());
        command.add(runningJar.getAbsolutePath());
        command.add(temporaryStorage.getAbsolutePath());

        if (args != null && args.length > 0) {
            command.addAll(Arrays.asList(args));
        }

        System.out.println("Creating process");
        System.out.println(command.toString());
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        try {
            processBuilder.start();
        } catch (IOException ex) {
            Logger.getLogger(Updater.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        try {
            Thread.sleep(4 * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Updater.class.getName()).log(Level.SEVERE, null, ex);
        }
        String oldJarPath = args[0];
        String updateJarPath = args[1];
        String finalJarPath = oldJarPath.substring(0, oldJarPath.lastIndexOf(File.separator)) + updateJarPath.substring(updateJarPath.lastIndexOf(File.separator));
        List<String> startArgs = null;
        if (args.length > 2) {
            startArgs = Arrays.asList(args).subList(2, args.length);
        }
        try {
            FilesystemHelper.copyFile(updateJarPath, finalJarPath);
            new File(updateJarPath).deleteOnExit();
            if (!oldJarPath.equals(finalJarPath)) {
                new File(oldJarPath).deleteOnExit();
            }
        } catch (IOException ex) {
            Logger.getLogger(Updater.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<String> command = new LinkedList<>();
        command.add(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java");
        command.add("-jar");
        command.add(finalJarPath);
        if (startArgs != null && startArgs.size() > 0) {
            command.addAll(startArgs);
        }
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        try {
            processBuilder.start();
        } catch (IOException ex) {
            Logger.getLogger(Updater.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
