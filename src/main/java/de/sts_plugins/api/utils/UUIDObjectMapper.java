package de.sts_plugins.api.utils;

import de.sts_plugins.api.plugin.IManagedObject;
import java.util.HashMap;
import java.util.UUID;
import de.sts_plugins.api.plugin.IAddon;

/**
 *
 * @author BR 89
 */
public class UUIDObjectMapper {
    private static UUIDObjectMapper instance;
    private final HashMap<UUID, IManagedObject> mapping = new HashMap<>();
    
    private UUIDObjectMapper() {
        
    }
    
    public static UUIDObjectMapper getInstance() {
        if (instance == null) {
            instance = new UUIDObjectMapper();
        }
        return instance;
    }
    
    public void clear() {
        mapping.clear();
    }
    
    public void add(UUID uuid, IManagedObject managedObject) {
        mapping.put(uuid, managedObject);
    }
    
    public void add(IManagedObject managedObject) {
        add(managedObject.getUUID(), managedObject);
    }
    
    public IManagedObject get(UUID uuid) {
        return mapping.get(uuid);
    }
    
    public UUID getRootUUID(UUID uuid) {
        IManagedObject object = get(uuid);
        if (object instanceof IAddon) {
            return getRootUUID(((IAddon) object).getParentUUID());
        }
        return uuid;
    }
}
