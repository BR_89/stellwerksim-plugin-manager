package de.sts_plugins.api.utils.rating;

/**
 *
 * @author BR 89
 */
public final class RatingObject {
    
    protected float rating;
    protected int numberOfRatings;

    public float getRating() {
        return this.rating;
    }

    public int getNumberOfRatings() {
        return this.numberOfRatings;
    }
    
}
