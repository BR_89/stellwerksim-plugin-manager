package de.sts_plugins.api.utils;

import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class UUIDStringMapper {
    protected static UUIDStringMapper instance;
    private final HashMap<UUID, String> mapping = new HashMap<>();
    
    private UUIDStringMapper() {
    }
    
    public String getString(UUID uuid) {
        return mapping.get(uuid);
    }
    
    public void addString(UUID uuid, String string) {
        mapping.put(uuid, string);
    }
    
    public boolean removeString(UUID uuid) {
        return (mapping.remove(uuid) != null);
    }
    
    public static UUIDStringMapper getInstance() {
        if (instance == null) {
            instance = new UUIDStringMapper();
        }
        return instance;
    }
}
