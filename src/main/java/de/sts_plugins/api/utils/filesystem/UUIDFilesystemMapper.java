package de.sts_plugins.api.utils.filesystem;

import de.sts_plugins.api.plugin.IAddon;
import de.sts_plugins.api.plugin.IManagedObject;
import java.io.File;
import java.util.HashMap;
import java.util.UUID;
import de.sts_plugins.api.plugin.IStellwerkAddon;
import de.sts_plugins.api.utils.UUIDObjectMapper;

/**
 *
 * @author BR 89
 */
public class UUIDFilesystemMapper {

    protected static UUIDFilesystemMapper instance;
    private final HashMap<UUID, String> mapping = new HashMap<>();

    private UUIDFilesystemMapper() {
    }

    public static UUIDFilesystemMapper getInstance() {
        if (instance == null) {
            instance = new UUIDFilesystemMapper();
        }
        return instance;
    }

    public void addPath(UUID uuid, String path) throws IllegalArgumentException {
        mapping.put(uuid, path);
    }

    public String getPath(UUID uuid) {
        IManagedObject object = UUIDObjectMapper.getInstance().get(uuid);
        if (object instanceof IStellwerkAddon) {
            if (((IStellwerkAddon) object).getAid() > 0) {
                return new File(
                        getBase(UUIDObjectMapper.getInstance().getRootUUID(uuid)),
                        "" + ((IStellwerkAddon) object).getAid()
                ).getPath();
            }
        }
        if (object instanceof IAddon) {
            if (((IAddon) object).getParentUUID() != null) {
                return mapping.get(UUIDObjectMapper.getInstance().getRootUUID(uuid));
            }
        }
        return mapping.get(uuid);
    }

    public File getBase(UUID uuid) {
        System.out.println(mapping.entrySet());
        return new File(getPath(uuid));
    }

    public ManagedFileSystemView getManagedFileSystemView(UUID uuid) {
        return new ManagedFileSystemView(getBase(uuid));
    }

}
