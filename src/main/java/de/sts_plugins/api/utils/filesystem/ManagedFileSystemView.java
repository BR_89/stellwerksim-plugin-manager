package de.sts_plugins.api.utils.filesystem;

import de.sts_plugins.api.utils.UUIDStringMapper;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author BR 89
 */
public class ManagedFileSystemView extends FileSystemView {

    private File[] roots;

    public ManagedFileSystemView(File root) {
        this(new File[]{root});
    }

    public ManagedFileSystemView(File[] roots) {
        this.roots = roots;
    }

    @Override
    public File createNewFolder(File containingDir) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getSystemDisplayName(File f) {
        String result = null;
        try {
            result = UUIDStringMapper.getInstance().getString(UUID.fromString(f.getName()));
        } catch (IllegalArgumentException ex) {

        }
        if (result == null || result.length() == 0) {
            result = super.getSystemDisplayName(f);
        }
        return result;
    }

    @Override
    public File getDefaultDirectory() {
        return roots[0];
    }

    @Override
    public File[] getRoots() {
        return this.roots;
    }

    @Override
    public boolean isRoot(File f) {
        for (File root : roots) {
            if (root.equals(f)) {
                return true;
            }
        }
        return false;
    }

}
