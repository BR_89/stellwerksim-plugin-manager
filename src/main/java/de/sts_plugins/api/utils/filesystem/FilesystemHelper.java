package de.sts_plugins.api.utils.filesystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author BR 89
 */
public class FilesystemHelper {

    public static byte[] asBytes(File resource) throws IOException {
        if (resource.exists() && resource.canRead()) {
            return Files.readAllBytes(resource.toPath());
        }
        return new byte[0];
    }

    public static String asString(File file) throws IOException {
        return new String(asBytes(file), "UTF-8");
    }

    public static void ensureDirectory(String path) {
        ensureDirectory(new File(path));
    }

    public static void ensureDirectory(File resource) {
        resource.mkdirs();
    }

    public static void copyFile(String source, String destination) throws IOException {
        copyFile(new File(source), new File(destination));
    }

    public static void copyFile(File sourceFile, File destinationFile)
            throws IOException {
        copyFile(sourceFile, destinationFile, true);
    }

    public static void copyFile(File sourceFile, File destinationFile, boolean overwrite)
            throws IOException {
        if (sourceFile.exists()) {
            if (!destinationFile.exists()) {
                destinationFile.getParentFile().mkdirs();
                destinationFile.createNewFile();
            } else if (!overwrite) {
                return;
            }
            FileChannel sourceStream = null;
            FileChannel destinationStream = null;
            try {
                sourceStream = new FileInputStream(sourceFile).getChannel();
                destinationStream = new FileOutputStream(destinationFile).getChannel();
                destinationStream.transferFrom(sourceStream, 0L, sourceStream.size());
            } finally {
                if (sourceStream != null) {
                    sourceStream.close();
                }
                if (destinationStream != null) {
                    destinationStream.close();
                }
            }
        }
    }

    public static boolean delete(File resource)
            throws IOException {
        if (resource.isDirectory()) {
            File[] childFiles = resource.listFiles();
            for (File child : childFiles) {
                delete(child);
            }
        }
        return resource.delete();
    }

    public static void extractZipTo(String source, String destination) {
        Logger logger = Logger.getLogger(FilesystemHelper.class.getName());
        byte[] buffer = new byte[1024];
        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(source))) {
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            while (zipEntry != null) {
                // Handle current entry
                if (!zipEntry.isDirectory()) {
                    String entryName = zipEntry.getName();
                    new File(destination + File.separator + entryName).getParentFile().mkdirs();
                    try (FileOutputStream fileOutputStream = new FileOutputStream(destination + File.separator + entryName)) {
                        int n;
                        while ((n = zipInputStream.read(buffer, 0, 1024)) > -1) {
                            fileOutputStream.write(buffer, 0, n);
                        }
                    }
                }
                // Retrieve next entry
                zipInputStream.closeEntry();
                zipEntry = zipInputStream.getNextEntry();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
}
