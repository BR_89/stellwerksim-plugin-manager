package de.sts_plugins.api.utils;

import de.sts_plugins.api.utils.filesystem.FilesystemHelper;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class HashChecker {
    
    public static enum Type{
        MD5("md5"),
        SHA256("SHA-256");
        
        private final String type;
        
        private Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    };
    
    protected static String calculateByteHash(byte[] bytes, Type type) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(type.getType());
        return convertToHex(md.digest(bytes));
    }
    
    public static String calculateFileHash(File file, Type type) throws NoSuchAlgorithmException, IOException {
        return calculateByteHash(FilesystemHelper.asBytes(file), type);
    }

    public static String calculateTextHash(String text, Type type) throws NoSuchAlgorithmException {
        try {
            return calculateByteHash(text.getBytes("UTF-8"), type);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HashChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static String convertToHex(byte[] raw) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < raw.length; i++) {
            sb.append(Integer.toString((raw[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
