package de.sts_plugins.api.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class PropertiesManager {
    public static Properties readFilesystem(String path) {
        return readFilesystem(path, null);
    }

    public static Properties readFilesystem(String path, Properties defaults) {
        Properties properties = new Properties(defaults);
        try (FileInputStream fis = new FileInputStream(path)) {
            properties.load(fis);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return defaults;
        }
        return properties;
    }
    
    public static Properties readClasspath(String path) {
        return readClasspath(path, null);
    }
    
    public static Properties readClasspath(String path, Properties defaults) {
        Properties properties = new Properties(defaults);
        try (InputStream is = PropertiesManager.class.getResourceAsStream(path)) {
            properties.load(is);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return properties;
    }
    
    public static boolean writeFilesystem(String path, Properties properties) {
        try (FileOutputStream fos = new FileOutputStream(path)) {
            properties.store(fos, "");
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
