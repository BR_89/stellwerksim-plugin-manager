package de.sts_plugins.api.utils;

import java.awt.Font;
import java.awt.GraphicsEnvironment;

/**
 *
 * @author BR 89
 */
public class FontUtils {
    
    public static void registerFont(Font font) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(font);
    }
    
}
