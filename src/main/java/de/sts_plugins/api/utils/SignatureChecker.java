package de.sts_plugins.api.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class SignatureChecker {
    

    public final byte[] data;
    public final byte[] signature;
    public final static String START_TOKEN = "-----BEGIN SIGNED DATA-----";
    public final static String MIDDLE_TOKEN = "-----BEGIN SIGNATURE-----";
    public final static String END_TOKEN = "-----END SIGNATURE-----";

    private SignatureChecker(byte[] data, byte[] signature) {
        this.data = data;
        this.signature = signature;
    }

    private boolean verify() {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            for (Byte[] signatur_bytes : Signatures.getSignatures()) {
                byte[] encodedKey = new byte[signatur_bytes.length];
                for (int i = 0; i < signatur_bytes.length; i++) {
                    encodedKey[i] = signatur_bytes[i];
                }
                X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedKey);
                try {
                    if (verify(keyFactory.generatePublic(publicKeySpec))) {
                        return true;
                    }
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(SignatureChecker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SignatureChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private boolean verify(PublicKey publicKey) {
        try {
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initVerify(publicKey);
            sig.update(data);
            return sig.verify(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException ex) {
            Logger.getLogger(SignatureChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean verify(byte[] data, byte[] signature) {
        return new SignatureChecker(data, signature).verify();
    }

    public static boolean verify(String string) {
        if (!string.contains(START_TOKEN) || !string.contains(MIDDLE_TOKEN) || !string.contains(END_TOKEN)) {
            return false;
        }
        return verify(getByteDataFromString(string), getByteSignatureFromString(string));
    }

    private static byte[] decode(String string) {
        try {
            return Base64.getDecoder().decode(string.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SignatureChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new byte[0];
    }

    public static byte[] getByteDataFromString(String string) {
        return decode(string.substring(string.indexOf(START_TOKEN) + START_TOKEN.length(), string.indexOf(MIDDLE_TOKEN)).trim());
    }

    public static byte[] getByteSignatureFromString(String string) {
        return decode(string.substring(string.indexOf(MIDDLE_TOKEN) + MIDDLE_TOKEN.length(), string.indexOf(END_TOKEN)).trim());
    }

    public static String getStringDataFromString(String string) {
        try {
            return new String(getByteDataFromString(string), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SignatureChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

class Signatures {

    public final static Byte[] CURRENT = {
        (byte) 0x30, (byte) 0x82, (byte) 0x02, (byte) 0x22,
        (byte) 0x30, (byte) 0x0d, (byte) 0x06, (byte) 0x09,
        (byte) 0x2a, (byte) 0x86, (byte) 0x48, (byte) 0x86,
        (byte) 0xf7, (byte) 0x0d, (byte) 0x01, (byte) 0x01,
        (byte) 0x01, (byte) 0x05, (byte) 0x00, (byte) 0x03,
        (byte) 0x82, (byte) 0x02, (byte) 0x0f, (byte) 0x00,
        (byte) 0x30, (byte) 0x82, (byte) 0x02, (byte) 0x0a,
        (byte) 0x02, (byte) 0x82, (byte) 0x02, (byte) 0x01,
        (byte) 0x00, (byte) 0xf7, (byte) 0x04, (byte) 0xe8,
        (byte) 0x7c, (byte) 0x2a, (byte) 0x4d, (byte) 0x19,
        (byte) 0xd1, (byte) 0x75, (byte) 0xf9, (byte) 0xb5,
        (byte) 0xea, (byte) 0x37, (byte) 0xc1, (byte) 0x3e,
        (byte) 0x1c, (byte) 0x9e, (byte) 0x96, (byte) 0x10,
        (byte) 0xd0, (byte) 0xc2, (byte) 0x1f, (byte) 0x51,
        (byte) 0x2d, (byte) 0x77, (byte) 0x9c, (byte) 0x20,
        (byte) 0x56, (byte) 0x97, (byte) 0x93, (byte) 0x63,
        (byte) 0x14, (byte) 0xa5, (byte) 0x94, (byte) 0x35,
        (byte) 0x9b, (byte) 0x4d, (byte) 0xce, (byte) 0xf9,
        (byte) 0x50, (byte) 0x0c, (byte) 0xfe, (byte) 0x2b,
        (byte) 0x3c, (byte) 0xee, (byte) 0x4d, (byte) 0xb7,
        (byte) 0x01, (byte) 0xc6, (byte) 0x9d, (byte) 0xbb,
        (byte) 0x1a, (byte) 0x33, (byte) 0xfc, (byte) 0xa1,
        (byte) 0x31, (byte) 0x1b, (byte) 0x22, (byte) 0xce,
        (byte) 0x90, (byte) 0x42, (byte) 0x42, (byte) 0xa7,
        (byte) 0x7f, (byte) 0xfe, (byte) 0x08, (byte) 0x9a,
        (byte) 0x15, (byte) 0x38, (byte) 0x63, (byte) 0xd1,
        (byte) 0x64, (byte) 0xa9, (byte) 0x86, (byte) 0xa5,
        (byte) 0xbf, (byte) 0xdb, (byte) 0x78, (byte) 0x3c,
        (byte) 0x5a, (byte) 0x5b, (byte) 0x93, (byte) 0xe2,
        (byte) 0xda, (byte) 0x1d, (byte) 0xca, (byte) 0x7a,
        (byte) 0x57, (byte) 0x97, (byte) 0x46, (byte) 0x7a,
        (byte) 0x9a, (byte) 0xdf, (byte) 0x8b, (byte) 0x2c,
        (byte) 0x8e, (byte) 0x31, (byte) 0x85, (byte) 0x79,
        (byte) 0x05, (byte) 0x71, (byte) 0xbd, (byte) 0x9d,
        (byte) 0x16, (byte) 0x50, (byte) 0x7c, (byte) 0x98,
        (byte) 0xf8, (byte) 0x29, (byte) 0xa0, (byte) 0x4f,
        (byte) 0xc2, (byte) 0xe4, (byte) 0x1e, (byte) 0x23,
        (byte) 0x6c, (byte) 0x8c, (byte) 0x59, (byte) 0x44,
        (byte) 0xb3, (byte) 0xa5, (byte) 0x81, (byte) 0xc8,
        (byte) 0x40, (byte) 0x75, (byte) 0x93, (byte) 0xd0,
        (byte) 0x5a, (byte) 0xf7, (byte) 0x77, (byte) 0x7c,
        (byte) 0xc6, (byte) 0x01, (byte) 0x87, (byte) 0xe8,
        (byte) 0xd0, (byte) 0x89, (byte) 0x7a, (byte) 0x29,
        (byte) 0xd7, (byte) 0x48, (byte) 0x4b, (byte) 0x11,
        (byte) 0x24, (byte) 0x68, (byte) 0x36, (byte) 0xae,
        (byte) 0x47, (byte) 0xbf, (byte) 0x80, (byte) 0x7e,
        (byte) 0xda, (byte) 0x43, (byte) 0xc9, (byte) 0x69,
        (byte) 0x91, (byte) 0x49, (byte) 0x2d, (byte) 0xb6,
        (byte) 0xe8, (byte) 0x46, (byte) 0x79, (byte) 0x2e,
        (byte) 0x79, (byte) 0x6b, (byte) 0x80, (byte) 0xde,
        (byte) 0xce, (byte) 0xfb, (byte) 0xfd, (byte) 0xd8,
        (byte) 0x12, (byte) 0x69, (byte) 0x22, (byte) 0x61,
        (byte) 0x79, (byte) 0x13, (byte) 0x04, (byte) 0x7e,
        (byte) 0x53, (byte) 0x90, (byte) 0xcf, (byte) 0x3c,
        (byte) 0x43, (byte) 0x6f, (byte) 0x7a, (byte) 0xc5,
        (byte) 0x6e, (byte) 0x82, (byte) 0xc5, (byte) 0x3a,
        (byte) 0x4f, (byte) 0xd1, (byte) 0xa7, (byte) 0x6e,
        (byte) 0xfc, (byte) 0xf8, (byte) 0x9e, (byte) 0x09,
        (byte) 0x05, (byte) 0xee, (byte) 0x27, (byte) 0x7b,
        (byte) 0x0c, (byte) 0xa9, (byte) 0xb8, (byte) 0xe8,
        (byte) 0x14, (byte) 0x24, (byte) 0x8c, (byte) 0xb9,
        (byte) 0x8f, (byte) 0xcb, (byte) 0xf9, (byte) 0xf0,
        (byte) 0x59, (byte) 0xaa, (byte) 0x58, (byte) 0xad,
        (byte) 0x40, (byte) 0x8c, (byte) 0x5e, (byte) 0xfb,
        (byte) 0x74, (byte) 0xd3, (byte) 0x4f, (byte) 0x39,
        (byte) 0x62, (byte) 0xe3, (byte) 0x76, (byte) 0xf4,
        (byte) 0x1a, (byte) 0x76, (byte) 0xdc, (byte) 0x41,
        (byte) 0x4d, (byte) 0x0c, (byte) 0x3e, (byte) 0x49,
        (byte) 0x3c, (byte) 0xb7, (byte) 0xa4, (byte) 0xb8,
        (byte) 0xca, (byte) 0xb8, (byte) 0x05, (byte) 0x00,
        (byte) 0x3d, (byte) 0x38, (byte) 0xa7, (byte) 0x1f,
        (byte) 0xb5, (byte) 0x8b, (byte) 0xb7, (byte) 0x09,
        (byte) 0x07, (byte) 0x26, (byte) 0x44, (byte) 0x55,
        (byte) 0x46, (byte) 0x6c, (byte) 0xc1, (byte) 0x2a,
        (byte) 0x9c, (byte) 0x69, (byte) 0x5a, (byte) 0x5f,
        (byte) 0xa3, (byte) 0x73, (byte) 0x08, (byte) 0xbd,
        (byte) 0x07, (byte) 0x57, (byte) 0x1b, (byte) 0x9c,
        (byte) 0x3c, (byte) 0xb3, (byte) 0x80, (byte) 0x8d,
        (byte) 0x67, (byte) 0xc7, (byte) 0xfe, (byte) 0x00,
        (byte) 0x30, (byte) 0x86, (byte) 0x65, (byte) 0x03,
        (byte) 0x67, (byte) 0x3c, (byte) 0xd4, (byte) 0xbd,
        (byte) 0x92, (byte) 0x26, (byte) 0x6a, (byte) 0x8a,
        (byte) 0xc8, (byte) 0x41, (byte) 0x8c, (byte) 0x26,
        (byte) 0xe1, (byte) 0xe6, (byte) 0x22, (byte) 0x3a,
        (byte) 0xdb, (byte) 0x58, (byte) 0x1f, (byte) 0x72,
        (byte) 0x15, (byte) 0x29, (byte) 0x00, (byte) 0xbe,
        (byte) 0xef, (byte) 0x52, (byte) 0x65, (byte) 0xb0,
        (byte) 0xbd, (byte) 0xec, (byte) 0x09, (byte) 0x7d,
        (byte) 0x82, (byte) 0x6d, (byte) 0xd8, (byte) 0x42,
        (byte) 0x57, (byte) 0x85, (byte) 0xaf, (byte) 0x68,
        (byte) 0xec, (byte) 0xfe, (byte) 0x4a, (byte) 0x00,
        (byte) 0xdc, (byte) 0x28, (byte) 0x1d, (byte) 0xf2,
        (byte) 0x14, (byte) 0xe1, (byte) 0xb4, (byte) 0x84,
        (byte) 0x95, (byte) 0x47, (byte) 0x39, (byte) 0xce,
        (byte) 0x11, (byte) 0xd7, (byte) 0xd1, (byte) 0x5a,
        (byte) 0xd3, (byte) 0xf8, (byte) 0xc3, (byte) 0xff,
        (byte) 0xa7, (byte) 0x57, (byte) 0x0a, (byte) 0x38,
        (byte) 0xad, (byte) 0x58, (byte) 0x16, (byte) 0xdd,
        (byte) 0x36, (byte) 0xae, (byte) 0x45, (byte) 0xb7,
        (byte) 0x61, (byte) 0xa9, (byte) 0x3c, (byte) 0xd4,
        (byte) 0x08, (byte) 0xb6, (byte) 0x76, (byte) 0xde,
        (byte) 0x95, (byte) 0x06, (byte) 0x7a, (byte) 0x5c,
        (byte) 0xe7, (byte) 0xb8, (byte) 0x6f, (byte) 0x6f,
        (byte) 0xef, (byte) 0x88, (byte) 0x21, (byte) 0xf9,
        (byte) 0x6d, (byte) 0xbd, (byte) 0x23, (byte) 0x09,
        (byte) 0x63, (byte) 0x7e, (byte) 0x4e, (byte) 0xb5,
        (byte) 0x97, (byte) 0x7c, (byte) 0xc3, (byte) 0x21,
        (byte) 0x3c, (byte) 0xf0, (byte) 0x6e, (byte) 0x0c,
        (byte) 0x27, (byte) 0x80, (byte) 0x9c, (byte) 0x28,
        (byte) 0xa2, (byte) 0xed, (byte) 0x75, (byte) 0xc4,
        (byte) 0x95, (byte) 0x25, (byte) 0x82, (byte) 0x18,
        (byte) 0xc0, (byte) 0x09, (byte) 0x2d, (byte) 0xf0,
        (byte) 0x93, (byte) 0x40, (byte) 0x6b, (byte) 0xf3,
        (byte) 0x05, (byte) 0x9f, (byte) 0x9c, (byte) 0x9e,
        (byte) 0x8f, (byte) 0x4e, (byte) 0x4f, (byte) 0x54,
        (byte) 0x04, (byte) 0xac, (byte) 0x45, (byte) 0x9f,
        (byte) 0x77, (byte) 0x8f, (byte) 0x26, (byte) 0x36,
        (byte) 0x10, (byte) 0xcb, (byte) 0xdd, (byte) 0xc4,
        (byte) 0xb6, (byte) 0x14, (byte) 0x69, (byte) 0xdc,
        (byte) 0x91, (byte) 0x5c, (byte) 0xaa, (byte) 0xc7,
        (byte) 0x92, (byte) 0xa5, (byte) 0xc7, (byte) 0xe5,
        (byte) 0xd8, (byte) 0x81, (byte) 0xca, (byte) 0x47,
        (byte) 0xe2, (byte) 0x27, (byte) 0x93, (byte) 0x66,
        (byte) 0xa5, (byte) 0xe6, (byte) 0xd9, (byte) 0xda,
        (byte) 0x12, (byte) 0x96, (byte) 0xc4, (byte) 0xed,
        (byte) 0x40, (byte) 0x78, (byte) 0xc1, (byte) 0x18,
        (byte) 0xfe, (byte) 0x34, (byte) 0xf4, (byte) 0x82,
        (byte) 0xb4, (byte) 0x8c, (byte) 0x1a, (byte) 0x7f,
        (byte) 0x5b, (byte) 0xa3, (byte) 0x0f, (byte) 0xae,
        (byte) 0x0e, (byte) 0x55, (byte) 0xaa, (byte) 0x63,
        (byte) 0xa4, (byte) 0x09, (byte) 0xc8, (byte) 0xac,
        (byte) 0xe0, (byte) 0x9c, (byte) 0x53, (byte) 0xf4,
        (byte) 0x82, (byte) 0x27, (byte) 0x8e, (byte) 0x22,
        (byte) 0xc2, (byte) 0x3b, (byte) 0x04, (byte) 0xb9,
        (byte) 0x20, (byte) 0x9b, (byte) 0xa5, (byte) 0x96,
        (byte) 0x03, (byte) 0xbe, (byte) 0xc7, (byte) 0x7b,
        (byte) 0xf1, (byte) 0x02, (byte) 0x03, (byte) 0x01,
        (byte) 0x00, (byte) 0x01 };

    public static List<Byte[]> getSignatures() {
        ArrayList<Byte[]> list = new ArrayList<>();
        list.add(CURRENT);
        return list;
    }
}
