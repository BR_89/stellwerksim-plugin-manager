package de.sts_plugins.manager.sim.event;

import de.sts_plugins.manager.sim.StellwerkInfo;

/**
 *
 * @author BR 89
 */
public class SimConnected {
    public final StellwerkInfo currentStellwerk;

    public SimConnected(StellwerkInfo currentStellwerk) {
        this.currentStellwerk = currentStellwerk;
    }
}
