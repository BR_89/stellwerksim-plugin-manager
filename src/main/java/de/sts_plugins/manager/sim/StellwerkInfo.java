package de.sts_plugins.manager.sim;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.sts_plugins.api.utils.Downloader;
import de.sts_plugins.api.utils.SignatureChecker;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.plugin.PluginManager;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class StellwerkInfo {

    public final String name;
    public final String region;
    public final int aid;
    public final int rid;

    public StellwerkInfo(int aid) {
        this(null, aid);
    }

    public StellwerkInfo(String name, int aid) {
        this.aid = aid;
        String tName = name;
        String tRegion = null;
        int tRid = -1;

        JsonObject data = getAidData();
        if (data != null) {
            if (data.has("name")) {
                tName = data.get("name").getAsString();
            }
            if (data.has("rid")) {
                tRid = data.get("rid").getAsInt();
            }
            if (data.has("region")) {
                tRegion = data.get("region").getAsString();
            }
        }
        this.name = tName;
        this.region = tRegion;
        this.rid = tRid;
    }

    @Override
    public String toString() {
        return name + " (" + aid + ")";
    }

    protected final JsonObject getAidData() {
        try {
            String data = new String(Downloader.downloadAsByteArray(new URL(String.format("%s/aid/%d", StellwerkSimPluginManager.getInstance().HOST, this.aid))), "UTF-8");
            if (SignatureChecker.verify(data)) {
                return new JsonParser().parse(SignatureChecker.getStringDataFromString(data)).getAsJsonObject();
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(StellwerkInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
