package de.sts_plugins.manager.sim;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author BR 89
 */
public class CurrentStellwerk extends StellwerkInfo {

    private static String getCurrentName(String xml) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(xml)));
            return doc.getFirstChild().getAttributes().getNamedItem("name").getTextContent();
        } catch (NullPointerException ex) {
            Logger.getLogger(StellwerkSimConnection.class.getName()).log(Level.FINER, null, ex);
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(StellwerkSimConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static int getCurrentAid(String xml) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(xml)));
            return Integer.parseInt(doc.getFirstChild().getAttributes().getNamedItem("aid").getTextContent());
        } catch (NullPointerException ex) {
            Logger.getLogger(StellwerkSimConnection.class.getName()).log(Level.FINER, null, ex);
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(StellwerkSimConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public CurrentStellwerk(String xml) {
        super(getCurrentName(xml), getCurrentAid(xml));
    }
}
