package de.sts_plugins.manager.sim;

import de.sts_plugins.api.utils.PropertiesManager;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.sim.event.SimConnected;
import de.sts_plugins.manager.sim.event.SimDisconnected;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class StellwerkSimConnection implements Runnable {

    private static StellwerkSimConnection instance;
    public static int PORT;
    protected boolean connected;
    protected CurrentStellwerk currentStellwerk;
    protected Thread thread;

    private StellwerkSimConnection() {
        Properties properties = PropertiesManager.readClasspath("/StellwerkSimPluginManager.properties");
        PORT = Integer.parseInt(properties.getProperty("plugin_port", "3691"));
    }

    public boolean isConnected() {
        return connected;
    }

    public void close() {
        this.connected = false;
        this.thread = null;
        StellwerkSimPluginManager.EVENT_BUS.post(new SimDisconnected());
    }

    public void open() {
        if (this.connected) {
            return;
        }
        currentStellwerk = null;
        this.connected = true;
        this.thread = new Thread(this);
        this.thread.start();
    }

    @Override
    public void run() {
        try (Socket socket = new Socket("localhost", PORT)) {
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            BufferedWriter output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
            input.readLine();
            output.write("<register name=\"StellwerkSim-Plugin-Manager\" autor=\"BR 89\" version=\"" + StellwerkSimPluginManager.getInstance().VERSION + "\" protokoll=\"1\" text=\"\" />\n");
            output.flush();
            input.readLine();
            output.write("<anlageninfo />\n");
            output.flush();
            StellwerkSimPluginManager.EVENT_BUS.post(new SimConnected(new CurrentStellwerk(input.readLine())));
            while(true) {
                input.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(StellwerkSimConnection.class.getName()).log(Level.INFO, null, ex);
            getInstance().thread = null;
            getInstance().close();
        }
    }

    public static StellwerkSimConnection getInstance() {
        if (instance == null) {
            instance = new StellwerkSimConnection();
        }
        return instance;
    }

}
