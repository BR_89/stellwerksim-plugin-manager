package de.sts_plugins.manager.plugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.sts_plugins.api.utils.Downloader;
import de.sts_plugins.api.utils.rating.RatingRetriever;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class PluginRetriever {
    

    private final String uri;

    public PluginRetriever(String uri) {
        this.uri = uri;
    }
    
    public Plugin getPlugin(UUID uuid) {
        Plugin result = null;
        try {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            result = gson.fromJson(new String(Downloader.downloadAsByteArray(getUrlForUUID(uuid)), "UTF-8"), Plugin.class);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RatingRetriever.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private URL getUrlForUUID(UUID uuid) {
        try {
            return new URL(String.format(this.uri,uuid.toString()));
        } catch (MalformedURLException ex) {
            Logger.getLogger(RatingRetriever.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
