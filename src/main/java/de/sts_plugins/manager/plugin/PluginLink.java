package de.sts_plugins.manager.plugin;

import java.net.URL;

/**
 *
 * @author BR 89
 */
public class PluginLink {
    protected String name;
    protected URL url;

    PluginLink(String name, URL url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
    
    
}
