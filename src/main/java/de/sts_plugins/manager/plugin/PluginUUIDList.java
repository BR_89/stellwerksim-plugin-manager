package de.sts_plugins.manager.plugin;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.gui.event.ProgressEvent;
import de.sts_plugins.manager.plugin.event.PluginFilterChangedEvent;
import de.sts_plugins.manager.plugin.event.PluginListChangedEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.swing.AbstractListModel;

/**
 *
 * @author BR 89
 */
public class PluginUUIDList extends AbstractListModel {

    protected List<UUID> list = new LinkedList<>();

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public Object getElementAt(int index) {
        return list.get(index);
    }

    @Subscribe
    public void pluginFilterChanged(PluginFilterChangedEvent event) {
        ProgressEvent pe = new ProgressEvent();
        pe.progress = -1;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
        int oldSize = list.size();
        list.clear();
        this.fireIntervalRemoved(list, 0, oldSize);
        PluginManager.getInstance().getFilteredPluginList(event.filter).stream().forEach((object) -> {
            list.add(object.getUUID());
        });
        this.fireIntervalAdded(list, 0, this.getSize());
        pe = new ProgressEvent();
        pe.done = true;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginListChangedEvent());
    }

}
