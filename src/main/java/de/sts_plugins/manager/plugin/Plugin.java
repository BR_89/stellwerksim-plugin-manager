package de.sts_plugins.manager.plugin;

import de.sts_plugins.api.plugin.Requirement;
import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.api.plugin.IStellwerkAddon;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class Plugin implements IManagedObject, IStellwerkAddon {

    protected UUID uuid;
    protected String name;
    protected String version;
    protected Requirement[] requirements;
    protected PluginLink[] links;
    protected int aid;
    protected UUID parent;
    protected String infotext;
    protected boolean startable;
    protected boolean favorite;

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public Requirement[] getRequirements() {
        return requirements;
    }

    public PluginLink[] getLinks() {
        return links;
    }

    @Override
    public int getAid() {
        return aid;
    }

    @Override
    public UUID getParentUUID() {
        return parent;
    }

    public String getInfotext() {
        return infotext;
    }

    public boolean isStartable() {
        return startable;
    }

    public boolean isFavorite() {
        return favorite;
    }

    @Override
    public String toString() {
        return "<" + this.name + "@" + this.version + "::" + this.uuid.toString() + ">";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Plugin) {
            return ((Plugin) obj).uuid.equals(this.uuid);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.uuid);
        return hash;
    }

}
