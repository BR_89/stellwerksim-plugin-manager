package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class AbstractPluginEvent {
    public final UUID uuid;

    public AbstractPluginEvent(UUID uuid) {
        this.uuid = uuid;
    }
    
    
}
