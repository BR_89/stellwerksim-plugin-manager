package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class PluginInstalledEvent extends AbstractPluginEvent {
    
    public PluginInstalledEvent(UUID uuid) {
        super(uuid);
    }
    
}
