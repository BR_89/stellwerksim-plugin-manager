package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class FavoritePluginEvent extends AbstractPluginEvent {
    
    public FavoritePluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
