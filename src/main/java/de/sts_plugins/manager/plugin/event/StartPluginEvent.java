package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class StartPluginEvent extends AbstractPluginEvent {
    
    public StartPluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
