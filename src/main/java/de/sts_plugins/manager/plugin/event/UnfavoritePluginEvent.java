package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class UnfavoritePluginEvent extends AbstractPluginEvent {
    
    public UnfavoritePluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
