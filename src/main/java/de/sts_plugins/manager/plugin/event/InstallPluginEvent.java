package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class InstallPluginEvent extends AbstractPluginEvent {
    
    public InstallPluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
