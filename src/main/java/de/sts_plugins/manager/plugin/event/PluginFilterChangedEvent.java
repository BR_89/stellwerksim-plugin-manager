package de.sts_plugins.manager.plugin.event;

/**
 *
 * @author BR 89
 */
public class PluginFilterChangedEvent {

    public final String filter;

    public PluginFilterChangedEvent(String filter) {
        this.filter = filter;
    }

}
