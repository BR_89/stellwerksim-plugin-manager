package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class PluginSelectedEvent extends AbstractPluginEvent {

    public PluginSelectedEvent(UUID uuid) {
        super(uuid);
    }

}
