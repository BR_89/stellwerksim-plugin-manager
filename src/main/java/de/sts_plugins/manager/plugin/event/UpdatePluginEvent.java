package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class UpdatePluginEvent extends AbstractPluginEvent{
    
    public UpdatePluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
