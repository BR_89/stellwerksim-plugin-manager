package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class PluginUninstalledEvent extends AbstractPluginEvent {
    
    public PluginUninstalledEvent(UUID uuid) {
        super(uuid);
    }
    
}
