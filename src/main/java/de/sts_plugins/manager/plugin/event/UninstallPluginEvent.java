package de.sts_plugins.manager.plugin.event;

import java.util.UUID;

/**
 *
 * @author BR 89
 */
public class UninstallPluginEvent extends AbstractPluginEvent {
    
    public UninstallPluginEvent(UUID uuid) {
        super(uuid);
    }
    
}
