package de.sts_plugins.manager.plugin;

import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.sts_plugins.api.plugin.Requirement;
import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.api.utils.Downloader;
import de.sts_plugins.api.utils.SignatureChecker;
import de.sts_plugins.api.utils.UUIDObjectMapper;
import de.sts_plugins.api.utils.VersionComperator;
import de.sts_plugins.api.utils.filesystem.FilesystemHelper;
import de.sts_plugins.api.utils.filesystem.UUIDFilesystemMapper;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.init.InitializationEvent;
import de.sts_plugins.manager.event.init.PreInitializationEvent;
import de.sts_plugins.manager.gui.event.ProgressEvent;
import de.sts_plugins.manager.plugin.event.FavoritePluginEvent;
import de.sts_plugins.manager.plugin.event.InstallPluginEvent;
import de.sts_plugins.manager.plugin.event.PluginInstalledEvent;
import de.sts_plugins.manager.plugin.event.PluginSelectedEvent;
import de.sts_plugins.manager.plugin.event.PluginUninstalledEvent;
import de.sts_plugins.manager.plugin.event.StartPluginEvent;
import de.sts_plugins.manager.plugin.event.UnfavoritePluginEvent;
import de.sts_plugins.manager.plugin.event.UninstallPluginEvent;
import de.sts_plugins.manager.plugin.event.UpdatePluginEvent;
import de.sts_plugins.manager.plugin.filter.All;
import de.sts_plugins.manager.plugin.filter.Current;
import de.sts_plugins.manager.plugin.filter.Installed;
import de.sts_plugins.manager.plugin.filter.Updateable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class PluginManager {

    private static PluginManager instance;
    private final List<IManagedObject> remote = new ArrayList<>();
    private final List<IManagedObject> local = new ArrayList<>();
    protected URL remoteSource;
    protected File localSource;

    private PluginManager() {
        ManagedObjectFilterRegistry mofr = ManagedObjectFilterRegistry.getInstance();
        StellwerkSimPluginManager.EVENT_BUS.register(mofr);
        Current current = new Current();
        StellwerkSimPluginManager.EVENT_BUS.register(current);
        mofr.addElement(new All());
        mofr.addElement(new Installed());
        mofr.addElement(new Updateable());
        mofr.addElement(current);
    }

    @Subscribe
    public void preInit(PreInitializationEvent event) {
        this.localSource = new File((String) StellwerkSimPluginManager.getInstance().getProperties().get("installed_file"));
        try {
            this.remoteSource = new URL(String.format("%s/plugins/json", StellwerkSimPluginManager.getInstance().HOST));
        } catch (MalformedURLException ex) {
            Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Subscribe
    public void init(InitializationEvent event) {
        try {
            if (localSource.exists()) {
                loadPlugins(local, true);
            }
        } catch (NullPointerException ex) {
            Logger.getLogger(StellwerkSimPluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        writeInstalledPlugins();
        loadPlugins(remote, false);
    }

    public static PluginManager getInstance() {
        if (instance == null) {
            instance = new PluginManager();
        }
        return instance;
    }

    protected void handleJsonArray(JsonArray listData, List<IManagedObject> list) {
        list.clear();
        for (JsonElement jsonElement : listData) {
            if (jsonElement.isJsonObject()) {
                Plugin plugin = new Plugin();
                JsonObject jsonPlugin = jsonElement.getAsJsonObject();

                plugin.name = jsonPlugin.get("name").getAsString();
                plugin.uuid = UUID.fromString(jsonPlugin.get("uuid").getAsString());
                plugin.version = jsonPlugin.get("version").getAsString();

                // optional data
                if (jsonPlugin.has("requirements")) {
                    JsonArray jsonRequirements = jsonPlugin.get("requirements").getAsJsonArray();
                    Requirement[] requirements = new Requirement[jsonRequirements.size()];
                    for (int i = 0; i < requirements.length; i++) {
                        JsonObject jsonRequirement = jsonRequirements.get(i).getAsJsonObject();
                        requirements[i] = new Requirement(
                                UUID.fromString(jsonRequirement.get("uuid").getAsString()),
                                jsonRequirement.get("version").getAsString());

                    }
                }
                if (jsonPlugin.has("links")) {
                    JsonArray jsonLinks = jsonPlugin.get("links").getAsJsonArray();
                    PluginLink[] links = new PluginLink[jsonLinks.size()];
                    for (int i = 0; i < links.length; i++) {
                        JsonObject jsonRequirement = jsonLinks.get(i).getAsJsonObject();
                        try {
                            links[i] = new PluginLink(
                                    jsonRequirement.get("name").getAsString(),
                                    new URL(jsonRequirement.get("url").getAsString()));
                        } catch (MalformedURLException ex) {
                            Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }

                plugin.aid = jsonPlugin.has("aid") ? jsonPlugin.get("aid").getAsInt() : -1;
                plugin.parent = jsonPlugin.has("parent") ? UUID.fromString(jsonPlugin.get("parent").getAsString()) : null;
                plugin.favorite = jsonPlugin.has("favorite") ? jsonPlugin.get("favorite").getAsBoolean() : false;
                list.add(plugin);
            }
        }
        UUIDObjectMapper.getInstance().clear();
        local.stream().forEach((plugin) -> {
            UUIDObjectMapper.getInstance().add(plugin);
            UUIDFilesystemMapper.getInstance().addPath(plugin.getUUID(), "./" + plugin.getUUID().toString());
        });
        remote.stream().forEach((plugin) -> {
            UUIDObjectMapper.getInstance().add(plugin);
        });
    }

    protected void loadPlugins(List<IManagedObject> list, boolean useLocal) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        JsonArray listData = null;
        if (useLocal && localSource != null) {
            try {
                listData = new JsonParser().parse(FilesystemHelper.asString(localSource)).getAsJsonArray();
            } catch (IOException ex) {
                Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (!useLocal) {
            try {
                String data = new String(Downloader.downloadAsByteArray(remoteSource), "UTF-8");
                if (SignatureChecker.verify(data)) {
                    listData = new JsonParser().parse(SignatureChecker.getStringDataFromString(data)).getAsJsonArray();
                }
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (listData != null) {
            handleJsonArray(listData, list);
        }
    }

    public void writeInstalledPlugins() {
        IManagedObject[] iManagedObjects = local.toArray(new IManagedObject[local.size()]);
        Plugin[] plugins = new Plugin[local.size()];
        for (int i = 0; i < plugins.length; i++) {
            plugins[i] = (Plugin) iManagedObjects[i];
        }
        System.out.println(Arrays.toString(plugins));
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String data = gson.toJson(plugins, Plugin[].class);
        try (FileOutputStream fos = new FileOutputStream(localSource)) {
            fos.write(data.getBytes("UTF-8"));
        } catch (IOException ex) {
            Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<IManagedObject> getFilteredPluginList(String filterName) {
        ManagedObjectFilterRegistry mofr = ManagedObjectFilterRegistry.getInstance();
        if (mofr.hasFilter(filterName)) {
            List<IManagedObject> list = mofr.getFilter(filterName).getMatchingElements(remote);
            list.sort((IManagedObject o1, IManagedObject o2) -> {
                if (isFavorite(o1.getUUID()) && !isFavorite(o2.getUUID())) {
                    return 1;
                }
                else if (!isFavorite(o1.getUUID()) && isFavorite(o2.getUUID())) {
                    return -1;
                }
                return o1.getName().compareTo(o2.getName());
            });
            return list;
        }
        return new LinkedList<>();
    }

    public Plugin getPlugin(UUID uuid) {
        for (IManagedObject plugin : remote) {
            if (plugin.getUUID().equals(uuid)) {
                return (Plugin) plugin;
            }
        }
        return null;
    }

    public boolean isInstalled(UUID uuid) {
        return local.stream().anyMatch((iManagedObject) -> (iManagedObject.getUUID().equals(uuid)));
    }

    public boolean isFavorite(UUID uuid) {
        return local.stream().anyMatch((iManagedObject) -> (iManagedObject.getUUID().equals(uuid) && ((Plugin) iManagedObject).isFavorite()));
    }

    @Subscribe
    public void uninstall(UninstallPluginEvent event) {
        File resource = UUIDFilesystemMapper.getInstance().getBase(event.uuid);
        try {
            FilesystemHelper.delete(resource);
        } catch (IOException ex) {
            Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        local.remove(UUIDObjectMapper.getInstance().get(event.uuid));
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginUninstalledEvent(event.uuid));
    }

    @Subscribe
    public void uninstalled(PluginUninstalledEvent event) {
        this.writeInstalledPlugins();
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent(event.uuid));
    }

    @Subscribe
    public void install(InstallPluginEvent event) {
        ProgressEvent pe = new ProgressEvent();
        pe.progress = -1;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
        Plugin plugin = (Plugin) UUIDObjectMapper.getInstance().get(event.uuid);
        String path = "./" + event.uuid.toString();
        FilesystemHelper.ensureDirectory(path);
        UUIDFilesystemMapper.getInstance().addPath(event.uuid, path);
        local.add(UUIDObjectMapper.getInstance().get(event.uuid));
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginInstalledEvent(event.uuid));
        pe.done = true;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
    }

    @Subscribe
    public void installed(PluginInstalledEvent event) {
        this.writeInstalledPlugins();
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent(event.uuid));
    }
    
    @Subscribe
    public void favorited(FavoritePluginEvent event) {
        local.stream().filter((iManagedObject) -> (iManagedObject.getUUID().equals(event.uuid))).forEach((iManagedObject) -> {
            ((Plugin) iManagedObject).favorite = true;
        });
        this.writeInstalledPlugins();
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent(event.uuid));
    }
    
    @Subscribe
    public void unfavorited(UnfavoritePluginEvent event) {
        local.stream().filter((iManagedObject) -> (iManagedObject.getUUID().equals(event.uuid))).forEach((iManagedObject) -> {
            ((Plugin) iManagedObject).favorite = false;
        });
        this.writeInstalledPlugins();
        StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent(event.uuid));
    }

    @Subscribe
    public void update(UpdatePluginEvent event) {
        local.remove(UUIDObjectMapper.getInstance().get(event.uuid));
        install(new InstallPluginEvent(event.uuid));
    }

    @Subscribe
    public void start(StartPluginEvent event) {
        
    }

    public String getName(UUID plugin) {
        return UUIDObjectMapper.getInstance().get(plugin).getName();
    }

    public String getInfoText(UUID plugin) {
        return ((Plugin) UUIDObjectMapper.getInstance().get(plugin)).getInfotext();
    }

    public boolean hasUpdate(UUID plugin) {
        if (!isInstalled(plugin)) {
            return false;
        }
        return VersionComperator.compareVersionStrings(
                getInstalledVersion(plugin), getNewestVersion(plugin)) < 0;
    }

    public String getInstalledVersion(UUID plugin) {
        for (IManagedObject iManagedObject : local) {
            if (iManagedObject.getUUID().equals(plugin)) {
                return iManagedObject.getVersion();
            }
        }
        return null;
    }

    public String getNewestVersion(UUID plugin) {
        for (IManagedObject iManagedObject : remote) {
            if (iManagedObject.getUUID().equals(plugin)) {
                return iManagedObject.getVersion();
            }
        }
        return null;
    }

    public boolean isStartable(UUID plugin) {
        return ((Plugin) UUIDObjectMapper.getInstance().get(plugin)).isStartable();
    }

    public boolean areRequirementsFullfilled(UUID plugin) {
        Requirement[] requirements = getPlugin(plugin).getRequirements();
        if (requirements == null || requirements.length == 0) {
            return true;
        }
        for (Requirement requirement : requirements) {
            if (!isInstalled(requirement.getUuid())) {
                return false;
            }
            if (VersionComperator.versionInIntervalls(getInstalledVersion(plugin), requirement.getVersion())) {
                return false;
            }
        }
        return true;
    }

}
