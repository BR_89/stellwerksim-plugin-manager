package de.sts_plugins.manager.plugin;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.api.plugin.IManagedObjectFilter;
import de.sts_plugins.manager.plugin.filter.event.FilterObjectChanged;
import java.util.LinkedList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author BR 89
 */
public class ManagedObjectFilterRegistry extends AbstractListModel {

    private static ManagedObjectFilterRegistry instance;
    private final List<IManagedObjectFilter> list = new LinkedList<>();

    public static ManagedObjectFilterRegistry getInstance() {
        if (instance == null) {
            instance = new ManagedObjectFilterRegistry();
        }
        return instance;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public Object getElementAt(int index) {
        return list.get(index);
    }

    public void addElement(Object item) {
        if (item instanceof IManagedObjectFilter) {
            list.add((IManagedObjectFilter) item);
            this.fireIntervalAdded(item, getSize() - 1, getSize() - 1);
        }
    }

    public void removeElement(Object obj) {
        if (obj instanceof IManagedObjectFilter) {
            IManagedObjectFilter filter = (IManagedObjectFilter) obj;
            int index = list.indexOf(filter);
            if (index >= 0) {
                list.remove(filter);
                this.fireIntervalRemoved(obj, index, index);
            }
        }
    }

    public void insertElementAt(Object item, int index) {
        addElement(item);
    }

    public void removeElementAt(int index) {
        Object obj = list.get(index);
        list.remove(index);
        this.fireIntervalRemoved(obj, index, index);
    }
    
    public IManagedObjectFilter getFilter(String name) {
        for (IManagedObjectFilter filter : list) {
            if (filter.toString().equals(name)) {
                return filter;
            }
        }
        return null;
    }
    
    public boolean hasFilter(String name) {
        return list.stream().anyMatch((filter) -> (filter.toString().equals(name)));
    }
    
    @Subscribe
    public void filterObjectChanged(FilterObjectChanged event) {
        int index = list.indexOf(event.filter);
        System.out.println("Updating Combobox index: " + index);
        this.fireContentsChanged(this, index, index);
    }

}
