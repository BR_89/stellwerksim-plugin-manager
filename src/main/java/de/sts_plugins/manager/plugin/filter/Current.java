package de.sts_plugins.manager.plugin.filter;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.api.plugin.IManagedObjectFilter;
import de.sts_plugins.api.plugin.IStellwerkAddon;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.plugin.filter.event.FilterObjectChanged;
import de.sts_plugins.manager.sim.StellwerkInfo;
import de.sts_plugins.manager.sim.StellwerkSimConnection;
import de.sts_plugins.manager.sim.event.SimConnected;
import de.sts_plugins.manager.sim.event.SimDisconnected;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class Current implements IManagedObjectFilter {

    public StellwerkInfo currentStellwerk;

    @Override
    public String toString() {
        if (currentStellwerk != null) {
            return "Stellwerk " + currentStellwerk.toString();
        }
        return "Stellwerk";
    }

    @Subscribe
    public void simConnected(SimConnected event) {
        this.currentStellwerk = event.currentStellwerk;
        StellwerkSimPluginManager.EVENT_BUS.post(new FilterObjectChanged(this));
    }

    @Subscribe
    public void simDisconnected(SimDisconnected event) {
        this.currentStellwerk = null;
        StellwerkSimPluginManager.EVENT_BUS.post(new FilterObjectChanged(this));
    }

    @Override
    public List<IManagedObject> getMatchingElements(List<IManagedObject> input) {
        List<IManagedObject> result = new LinkedList<>();
        if (currentStellwerk == null) {
            int count = 0;
            StellwerkSimConnection.getInstance().open();
            while (count < 20 && currentStellwerk == null) {
                count++;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Current.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (currentStellwerk == null) {
                return result;
            }
        }
        input.stream().filter((iManagedObject) -> (iManagedObject instanceof IStellwerkAddon)).forEach((iManagedObject) -> {
            IStellwerkAddon isa = (IStellwerkAddon) iManagedObject;
            if (isa.getAid() == currentStellwerk.aid) {
                result.add(iManagedObject);
            }
        });
        return result;
    }

}
