package de.sts_plugins.manager.plugin.filter.event;

import de.sts_plugins.api.plugin.IManagedObjectFilter;

/**
 *
 * @author BR 89
 */
public class FilterObjectChanged {

    public final IManagedObjectFilter filter;

    public FilterObjectChanged(IManagedObjectFilter filter) {
        this.filter = filter;
    }

}
