package de.sts_plugins.manager.plugin.filter;

import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.api.plugin.IManagedObjectFilter;
import java.util.List;

/**
 *
 * @author BR 89
 */
public class All implements IManagedObjectFilter {

    @Override
    public String toString() {
        return "Alle";
    }

    @Override
    public List<IManagedObject> getMatchingElements(List<IManagedObject> input) {
        return input;
    }

}
