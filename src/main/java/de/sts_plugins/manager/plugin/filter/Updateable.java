package de.sts_plugins.manager.plugin.filter;

import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.api.plugin.IManagedObjectFilter;
import de.sts_plugins.manager.plugin.PluginManager;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BR 89
 */
public class Updateable implements IManagedObjectFilter {

    @Override
    public String toString() {
        return "Aktualisierbar";
    }

    @Override
    public List<IManagedObject> getMatchingElements(List<IManagedObject> input) {
        List<IManagedObject> result = new LinkedList<>();
        input.stream().filter((iManagedObject) -> (PluginManager.getInstance().hasUpdate(iManagedObject.getUUID()))).forEach((iManagedObject) -> {
            result.add(iManagedObject);
        });
        return result;
    }

}
