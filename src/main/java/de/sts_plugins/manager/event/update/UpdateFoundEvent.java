package de.sts_plugins.manager.event.update;

/**
 *
 * @author BR 89
 */
public class UpdateFoundEvent {

    public final String version;
    public final String url;
    public final String md5;
    public final String sha256;
    public final String[] args;

    public UpdateFoundEvent(String version, String url, String md5, String sha256, String[] args) {
        this.version = version;
        this.url = url;
        this.md5 = md5;
        this.sha256 = sha256;
        this.args = args;
    }

}
