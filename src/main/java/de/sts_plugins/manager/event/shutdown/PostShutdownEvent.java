package de.sts_plugins.manager.event.shutdown;

/**
 *
 * @author BR 89
 */
public class PostShutdownEvent {

    public final int exit;

    public PostShutdownEvent() {
        this.exit = 0;
    }

    public PostShutdownEvent(int exit) {
        this.exit = exit;
    }

}
