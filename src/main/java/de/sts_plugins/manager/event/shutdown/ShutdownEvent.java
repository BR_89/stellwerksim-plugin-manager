package de.sts_plugins.manager.event.shutdown;

/**
 *
 * @author BR 89
 */
public class ShutdownEvent {

    public final int exit;

    public ShutdownEvent(int exit) {
        this.exit = exit;
    }

    public ShutdownEvent() {
        this.exit = 0;
    }

}
