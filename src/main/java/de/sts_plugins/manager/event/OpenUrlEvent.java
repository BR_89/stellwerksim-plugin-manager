package de.sts_plugins.manager.event;

import java.net.URL;

/**
 *
 * @author BR 89
 */
public class OpenUrlEvent {

    public final URL url;

    public OpenUrlEvent(URL url) {
        this.url = url;
    }

}
