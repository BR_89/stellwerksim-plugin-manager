package de.sts_plugins.manager;

import de.sts_plugins.manager.event.update.UpdateFoundEvent;
import de.sts_plugins.manager.event.init.InitializationEvent;
import de.sts_plugins.manager.event.init.PreInitializationEvent;
import de.sts_plugins.manager.event.init.PostInitializationEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.sts_plugins.api.http.EndpointRegistry;
import de.sts_plugins.api.http.LetsEncryptCert;
import de.sts_plugins.api.utils.Downloader;
import de.sts_plugins.api.utils.HashChecker;
import de.sts_plugins.api.utils.PropertiesManager;
import de.sts_plugins.api.utils.SignatureChecker;
import de.sts_plugins.api.utils.Updater;
import de.sts_plugins.api.utils.VersionComperator;
import de.sts_plugins.manager.event.update.CheckUpdateEvent;
import de.sts_plugins.manager.event.shutdown.PostShutdownEvent;
import de.sts_plugins.manager.event.shutdown.PreShutdownEvent;
import de.sts_plugins.manager.event.shutdown.ShutdownEvent;
import de.sts_plugins.manager.event.update.NoUpdateFoundEvent;
import de.sts_plugins.manager.event.update.UpdateErrorEvent;
import de.sts_plugins.manager.gui.StellwerkSimPluginManagerJFrame;
import de.sts_plugins.manager.gui.TrayMenu;
import de.sts_plugins.manager.gui.font.FontAwesomeUtils;
import de.sts_plugins.manager.http.OpenUrlHandler;
import de.sts_plugins.manager.http.Server;
import de.sts_plugins.manager.http.endpoint.Shutdown;
import de.sts_plugins.manager.http.endpoint.Version;
import de.sts_plugins.manager.plugin.PluginManager;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class StellwerkSimPluginManager {

    public final String VERSION;
    public final String HOST;
    public final static EventBus EVENT_BUS = new EventBus();

    private static StellwerkSimPluginManager instance;
    private Server server;

    private StellwerkSimPluginManager() {
        Properties properties = PropertiesManager.readClasspath("/StellwerkSimPluginManager.properties");
        VERSION = properties.getProperty("version", "dev");
        HOST = properties.getProperty("host");
    }

    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("port", "8567");
        properties.setProperty("interface", "127.0.0.1");
        properties.setProperty("installed_file", "./installed.json");
        properties = PropertiesManager.readFilesystem("./stellwerksim-plugin-manager.properties", properties);
        PropertiesManager.writeFilesystem("./stellwerksim-plugin-manager.properties", properties);
        return properties;
    }

    public void startServer() {
        if (server == null) {
            EndpointRegistry.getInstance().addEndpoint(new Version());
            EndpointRegistry.getInstance().addEndpoint(new Shutdown());
            server = Server.getInstance();
            StellwerkSimPluginManager.EVENT_BUS.register(server);
            Properties properties = getProperties();
            server.setBindPort(Integer.parseInt(properties.getProperty("port")));
            server.setBindInterface(properties.getProperty("interface", "127.0.0.1"));
        }
        if (!server.isRunning()) {
            Thread t = new Thread(server);
            t.start();
        }
    }

    public void startGui() {
        FontAwesomeUtils.registerFont();
        StellwerkSimPluginManagerJFrame jFrame = StellwerkSimPluginManagerJFrame.getInstance();
        StellwerkSimPluginManager.EVENT_BUS.register(jFrame);
        jFrame.setVisible(true);
        TrayMenu.getInstance().add();
    }

    @Subscribe
    public void checkUpdate(CheckUpdateEvent event) {
        try {
            URL versionInfoUrl = new URL(String.format("%s/manager/json", HOST));
            String versionInfo = new String(
                    Downloader.downloadAsByteArray(versionInfoUrl), "UTF-8");
            if (SignatureChecker.verify(versionInfo)) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                StellwerkSimPluginManagerInfo sspmi = gson.fromJson(
                        SignatureChecker.getStringDataFromString(versionInfo),
                        StellwerkSimPluginManagerInfo.class);
                if (VersionComperator.compareVersionStrings(sspmi.version, VERSION) > 0) {
                    EVENT_BUS.post(new UpdateFoundEvent(sspmi.version, sspmi.url, sspmi.md5, sspmi.sha256, event.args));
                } else {
                    EVENT_BUS.post(new NoUpdateFoundEvent());
                }

            } else {
                EVENT_BUS.post(new UpdateErrorEvent());
            }
        } catch (MalformedURLException | UnsupportedEncodingException ex) {
            Logger.getLogger(StellwerkSimPluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Subscribe
    public void updateFound(UpdateFoundEvent event) {
        try {
            Updater updater = new Updater();
            updater.setMain(StellwerkSimPluginManager.class);
            updater.setSource(new URL(HOST + event.url));
            updater.addHash(HashChecker.Type.MD5, event.md5);
            updater.addHash(HashChecker.Type.SHA256, event.sha256);
            updater.setArgs(event.args);
            updater.run();
        } catch (MalformedURLException ex) {
            Logger.getLogger(StellwerkSimPluginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Subscribe
    public void noUpdateFound(NoUpdateFoundEvent event) {
        System.out.println("No newer Version found.");
    }

    @Subscribe
    public void updateError(UpdateErrorEvent event) {
        System.err.println("Error reading version info!");
    }

    @Subscribe
    public void shutdown(ShutdownEvent event) {
        StellwerkSimPluginManager.EVENT_BUS.post(new PreShutdownEvent());
        StellwerkSimPluginManager.EVENT_BUS.post(new PostShutdownEvent(event.exit));
    }

    @Subscribe
    public void postShutdown(PostShutdownEvent event) {
        System.exit(event.exit);
    }

    public static StellwerkSimPluginManager getInstance() {
        if (instance == null) {
            instance = new StellwerkSimPluginManager();
        }
        return instance;
    }

    public static void main(String[] args) {
        boolean startGui = true;
        boolean startServer = true;
        if (args.length > 0) {
            if (args.length > 1) {
                System.err.print("Invalid arguments");
                System.exit(1);
            }
            if ("--server".equals(args[0])) {
                startGui = false;
            }
            if ("--no-server".equals(args[0])) {
                startServer = false;
            }
        }

        // Remove this once LE is in java trust store on all systems.
        LetsEncryptCert.addLetsEncyptRootCA();

        // Start manager
        StellwerkSimPluginManager spm = StellwerkSimPluginManager.getInstance();
        StellwerkSimPluginManager.EVENT_BUS.register(spm);
        StellwerkSimPluginManager.EVENT_BUS.register(PluginManager.getInstance());
        if (startServer) {
            spm.startServer();
        }
        if (startGui) {
            spm.startGui();
        }
        StellwerkSimPluginManager.EVENT_BUS.register(new OpenUrlHandler());

        StellwerkSimPluginManager.EVENT_BUS.post(new CheckUpdateEvent(args));
        StellwerkSimPluginManager.EVENT_BUS.post(new PreInitializationEvent());
        StellwerkSimPluginManager.EVENT_BUS.post(new InitializationEvent());
        StellwerkSimPluginManager.EVENT_BUS.post(new PostInitializationEvent());
    }

    @Subscribe
    public void debug(Object event) {
        System.out.println(event);
    }
}

class StellwerkSimPluginManagerInfo {

    protected String version;
    protected String md5;
    protected String sha256;
    protected String url;
}
