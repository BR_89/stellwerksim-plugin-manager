package de.sts_plugins.manager.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import de.sts_plugins.api.http.IRequest;

/**
 *
 * @author BR 89
 */
public class Request implements IRequest {

    protected boolean invalid = false;
    protected String request;
    protected String query;
    protected Socket socket;

    public Request(Socket socket) throws IOException {
        this.socket = socket;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        request = bufferedReader.readLine();
        request = request.split(" ")[1];
        query = "";
        int index = request.indexOf('?');
        if (index >= 0) {
            query = request.substring(index);
            request = request.substring(0, index);
        }

        if (request.contains("..")) {
            invalid = true;
        }
    }

    @Override
    public boolean isValid() {
        return !invalid;
    }

    @Override
    public String getRequest() {
        return request;
    }

    @Override
    public String getQuery() {
        return query;
    }

}
