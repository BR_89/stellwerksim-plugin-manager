package de.sts_plugins.manager.http;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.event.OpenUrlEvent;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class OpenUrlHandler {

    @Subscribe
    public void openUrl(OpenUrlEvent event) {
        try {
            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(event.url.toURI());
            }
        } catch (URISyntaxException | IOException | NullPointerException ex) {
            Logger.getLogger(OpenUrlHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
