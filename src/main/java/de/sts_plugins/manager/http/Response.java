package de.sts_plugins.manager.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.sts_plugins.api.http.EndpointRegistry;
import de.sts_plugins.api.http.IEndpoint;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import de.sts_plugins.api.http.IResponse;

/**
 *
 * @author BR 89
 */
public class Response implements IResponse {

    protected Request request;
    protected String contentType;
    protected int code;
    String data = null;

    public Response(Request request) throws IOException {
        this.request = request;
        setContentType("application/json");
        setCode(404);
    }

    @Override
    public final void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public final void setCode(int code) {
        this.code = code;
    }

    @Override
    public void serializeToJson(Object object, Class clazz) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        data = gson.toJson(object, clazz);
        data = data.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
        data = data.replace("ä", "&auml;").replace("ö", "&ouml;").replace("ü", "&uuml;");
        data = data.replace("Ä", "&Auml;").replace("Ö", "&Ouml;").replace("Ü", "&Uuml;");
        data = data.replace("&", "&szlig;");
    }

    public void handle() throws IOException {
        IEndpoint endpoint = EndpointRegistry.getInstance().getEndpointForRequest(request);
        if (endpoint != null) {
            endpoint.handle(request, this);
        }
        sendData();
        request.socket.getOutputStream().flush();
        request.socket.close();
    }

    protected void sendHeader() throws IOException {
        PrintStream stream = new PrintStream(request.socket.getOutputStream());
        stream.print("HTTP/1.0 " + code + " " + codeText(code) + "\r\n"
                + "Content-Type: " + this.contentType + "\r\n"
                + "Date: " + new Date() + "\r\n\r\n");
    }

    void sendData() throws IOException {
        sendHeader();
        if (data == null) {
            data = "{\"code\": " + this.code + "}";
        }
        PrintStream stream = new PrintStream(request.socket.getOutputStream());
        stream.print(data + "\r\n\r\n");
    }

    private String codeText(int code) {
        switch (code) {
            case 200:
                return "OK";
            case 404:
                return "Not Found";
        }
        return "";
    }

}
