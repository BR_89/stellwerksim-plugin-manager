package de.sts_plugins.manager.http.endpoint;

import de.sts_plugins.api.http.IEndpoint;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.api.http.IRequest;
import de.sts_plugins.api.http.IResponse;

/**
 *
 * @author BR 89
 */
public class Version implements IEndpoint {

    @Override
    public boolean handles(IRequest request) {
        return "/version/".equals(request.getRequest());
    }

    @Override
    public void handle(IRequest request, IResponse response) {
        response.setCode(200);
        response.serializeToJson(new VersionInfo(StellwerkSimPluginManager.getInstance().VERSION), VersionInfo.class);
    }

}

class VersionInfo {

    protected String version;

    public VersionInfo(String version) {
        this.version = version;
    }
}
