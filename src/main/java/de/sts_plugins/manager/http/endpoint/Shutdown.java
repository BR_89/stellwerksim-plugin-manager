package de.sts_plugins.manager.http.endpoint;

import de.sts_plugins.api.http.IEndpoint;
import de.sts_plugins.api.http.IRequest;
import de.sts_plugins.api.http.IResponse;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.shutdown.ShutdownEvent;

/**
 *
 * @author BR 89
 */
public class Shutdown implements IEndpoint {

    @Override
    public boolean handles(IRequest request) {
        return "/shutdown/".equals(request.getRequest());
    }

    @Override
    public void handle(IRequest request, IResponse response) {
        response.setCode(200);
        StellwerkSimPluginManager.EVENT_BUS.post(new ShutdownEvent());
    }
}
