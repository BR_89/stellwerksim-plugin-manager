package de.sts_plugins.manager.http;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class Worker implements Runnable {

    protected final Socket connection;

    public Worker(Socket connection) {
        this.connection = connection;
    }

    @Override
    public void run() {
        try {
            Response response = new Response(new Request(connection));
            if (response.request.isValid()) {
                response.handle();
            }
        } catch (IOException ex) {
            Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
