package de.sts_plugins.manager.http;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.event.shutdown.PreShutdownEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BR 89
 */
public class Server implements Runnable {

    private static Server instance;
    private ServerSocket serverSocket;
    private final ExecutorService pool = Executors.newFixedThreadPool(4);
    private int bindPort;
    private String bindInterface;
    private boolean running;

    private Server() {
    }

    public void setBindInterface(String bindInterface) throws IllegalStateException {
        if (running) {
            throw new IllegalStateException("Server running");
        }
        this.bindInterface = bindInterface;
    }

    public void setBindPort(int bindPort) throws IllegalStateException {
        if (running) {
            throw new IllegalStateException("Server running");
        }
        this.bindPort = bindPort;
    }

    public int getBindPort() {
        return bindPort;
    }

    public boolean isRunning() {
        return running;
    }

    @Subscribe
    public void stop(PreShutdownEvent event) {
        running = false;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        running = true;
        System.out.println("Starting server");
        try {
            serverSocket = new ServerSocket(bindPort, 5, InetAddress.getByName(bindInterface));
            while (true) {
                final Socket socket = serverSocket.accept();
                pool.submit(new Worker(socket));
            }
        } catch (IOException ex) {
            if (running) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Shutting down server");
    }

    public static Server getInstance() {
        if (instance == null) {
            instance = new Server();
        }
        return instance;
    }

}
