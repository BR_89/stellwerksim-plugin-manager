package de.sts_plugins.manager.gui;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.OpenUrlEvent;
import de.sts_plugins.manager.event.shutdown.PreShutdownEvent;
import de.sts_plugins.manager.gui.event.ProgressEvent;
import de.sts_plugins.manager.gui.event.ProgressFinishedEvent;
import de.sts_plugins.manager.gui.font.FontAwesomeUtils;
import de.sts_plugins.manager.plugin.PluginManager;
import de.sts_plugins.manager.plugin.event.FavoritePluginEvent;
import de.sts_plugins.manager.plugin.event.InstallPluginEvent;
import de.sts_plugins.manager.plugin.event.PluginSelectedEvent;
import de.sts_plugins.manager.plugin.event.StartPluginEvent;
import de.sts_plugins.manager.plugin.event.UnfavoritePluginEvent;
import de.sts_plugins.manager.plugin.event.UninstallPluginEvent;
import de.sts_plugins.manager.plugin.event.UpdatePluginEvent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.UUID;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;

/**
 *
 * @author BR 89
 */
public class PluginInfoPanel extends javax.swing.JPanel {

    protected JScrollPane scrollComponent;
    protected JLabel titleComponent;
    protected JLabel versionComponent;
    protected JEditorPane descriptionComponent;
    protected JButton updateButton;
    protected JButton installButton;
    protected JButton startButton;
    protected JButton favoriteButton;
    protected UUID uuid;

    public PluginInfoPanel() {
        this.init();
    }

    private void init() {
        this.setLayout(new BorderLayout());
        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        installButton = new JButton("Herunterladen");
        installButton.addActionListener((ActionEvent e) -> {
            PluginManager pluginList = PluginManager.getInstance();
            if (pluginList.isInstalled(uuid)) {
                StellwerkSimPluginManager.EVENT_BUS.post(new UninstallPluginEvent(uuid));
            } else {
                StellwerkSimPluginManager.EVENT_BUS.post(new InstallPluginEvent(uuid));
            }
        });

        updateButton = new JButton("Aktualisieren");
        updateButton.addActionListener((ActionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new UpdatePluginEvent(uuid));
        });

        startButton = new JButton("Starten");
        startButton.addActionListener((ActionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new StartPluginEvent(uuid));
        });

        favoriteButton = new JButton("Favorite");
        favoriteButton.addActionListener((ActionEvent e) -> {
            PluginManager pluginList = PluginManager.getInstance();
            if (pluginList.isFavorite(uuid)) {
                StellwerkSimPluginManager.EVENT_BUS.post(new UnfavoritePluginEvent(uuid));
            } else {
                StellwerkSimPluginManager.EVENT_BUS.post(new FavoritePluginEvent(uuid));
            }
        });
        buttons.add(favoriteButton);
        buttons.add(updateButton);
        buttons.add(startButton);
        buttons.add(installButton);

        JPanel titlePanel = new JPanel(new BorderLayout());
        this.titleComponent = new JLabel("");
        Font font = titleComponent.getFont();
        this.titleComponent.setFont(new Font(font.getName(), font.getStyle(), font.getSize() * 3));
        this.versionComponent = new JLabel();
        titlePanel.add(titleComponent, BorderLayout.WEST);
        titlePanel.add(versionComponent, BorderLayout.EAST);

        this.descriptionComponent = new JEditorPane();
        this.descriptionComponent.setEditable(false);
        this.descriptionComponent.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
        this.descriptionComponent.addHyperlinkListener((HyperlinkEvent e) -> {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                StellwerkSimPluginManager.EVENT_BUS.post(new OpenUrlEvent(e.getURL()));
            }
        });
        this.scrollComponent = new JScrollPane(this.descriptionComponent);
        this.add(titlePanel, BorderLayout.NORTH);
        this.add(this.scrollComponent, BorderLayout.CENTER);
        this.add(buttons, BorderLayout.SOUTH);
        installButton.setEnabled(false);
        startButton.setEnabled(false);
        startButton.setVisible(false);
        updateButton.setEnabled(false);
        updateButton.setVisible(false);
        favoriteButton.setEnabled(false);
        favoriteButton.setVisible(false);
    }

    @Subscribe
    public void progress(ProgressEvent event) {
        this.installButton.setEnabled(false);
        this.startButton.setEnabled(false);
        this.updateButton.setEnabled(false);
        this.favoriteButton.setEnabled(false);
    }

    @Subscribe
    public void progressFinished(ProgressFinishedEvent event) {
        this.installButton.setEnabled(true);
        this.startButton.setEnabled(true);
        this.updateButton.setEnabled(true);
        this.favoriteButton.setEnabled(true);
        if (uuid != null) {
            this.setPlugin(uuid);
        }
    }

    @Subscribe
    public void preShutdown(PreShutdownEvent event) {
        this.descriptionComponent.setEnabled(false);
        this.installButton.setEnabled(false);
        this.scrollComponent.setEnabled(false);
        this.startButton.setEnabled(false);
        this.titleComponent.setEnabled(false);
        this.updateButton.setEnabled(false);
        this.versionComponent.setEnabled(false);
    }

    @Subscribe
    public void pluginSelected(PluginSelectedEvent event) {
        this.setPlugin(event.uuid);
    }

    public final void setPlugin(UUID uuid) {
        if (uuid == null) {
            this.titleComponent.setText("<html>" + FontAwesomeUtils.Icon.INFO.toHtml() + " Plugin auswählen</html>");
            this.versionComponent.setText(" ");
            this.descriptionComponent.setText("");
            installButton.setText("<html>" + FontAwesomeUtils.Icon.INFO.toHtml() + " Plugin auswählen</html>");
            installButton.setEnabled(false);
            startButton.setVisible(false);
            updateButton.setVisible(false);
            return;
        }
        PluginManager pm = PluginManager.getInstance();
        this.uuid = uuid;
        this.titleComponent.setText(pm.getName(uuid));
        this.descriptionComponent.setText(pm.getInfoText(uuid));
        installButton.setEnabled(true);
        if (pm.isInstalled(uuid)) {
            favoriteButton.setVisible(true);
            if (pm.isFavorite(uuid)) {
                favoriteButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.STAR_O.toHtml() + "&nbsp;</html>");
            } else {
                favoriteButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.STAR.toHtml() + "&nbsp;</html>");
            }
            if (pm.hasUpdate(uuid)) {
                updateButton.setVisible(true);
                this.versionComponent.setText(pm.getInstalledVersion(uuid) + " -> " + pm.getNewestVersion(uuid));
            } else {
                updateButton.setVisible(false);
                this.versionComponent.setText(pm.getNewestVersion(uuid));
            }
            startButton.setVisible(pm.isStartable(uuid));
            if (pm.areRequirementsFullfilled(uuid)) {
                startButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.PLAY.toHtml() + " Starten&nbsp;</html>");
                startButton.setEnabled(true);
            } else {
                startButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.WARNING.toHtml() + " Abhängigkeiten fehlen!&nbsp;</html>");
                startButton.setEnabled(false);
            }
            installButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.TRASH_O.toHtml() + " Entfernen&nbsp;</html>");
        } else {
            installButton.setText("<html>&nbsp;" + FontAwesomeUtils.Icon.DOWNLOAD.toHtml() + " Herunterladen&nbsp;</html>");
            favoriteButton.setVisible(false);
            startButton.setVisible(false);
            updateButton.setVisible(false);
            this.versionComponent.setText(pm.getNewestVersion(uuid));
        }
    }
}
