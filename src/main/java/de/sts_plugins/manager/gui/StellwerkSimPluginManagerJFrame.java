package de.sts_plugins.manager.gui;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.init.PostInitializationEvent;
import de.sts_plugins.manager.event.init.PreInitializationEvent;
import de.sts_plugins.manager.event.shutdown.PreShutdownEvent;
import de.sts_plugins.manager.event.shutdown.ShutdownEvent;
import de.sts_plugins.manager.gui.event.ProgressEvent;
import de.sts_plugins.manager.gui.font.FontAwesomeUtils;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontFormatException;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author BR 89
 */
public final class StellwerkSimPluginManagerJFrame extends javax.swing.JFrame {

    private static StellwerkSimPluginManagerJFrame instance;
    private PluginInfoPanel pluginInfoPanel;
    private PluginListPanel pluginListPanel;
    private CopyrightPanel copyrightPanel;

    private StellwerkSimPluginManagerJFrame() throws HeadlessException, FontFormatException, IOException {
        super("StellwerkSim-Plugin-Manager " + "::" + " " + StellwerkSimPluginManager.getInstance().VERSION);
        this.init();
    }
    
    private void init() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e) {
                StellwerkSimPluginManager.EVENT_BUS.post(new ShutdownEvent());
            }
        });
        setIconImage(new ImageIcon(getClass().getResource("/images/tray.png")).getImage());
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width / 2, screenSize.height / 2);
        setLocationRelativeTo(null);
        FontAwesomeUtils.registerFont();

        pluginInfoPanel = new PluginInfoPanel();
        StellwerkSimPluginManager.EVENT_BUS.register(pluginInfoPanel);
        pluginListPanel = new PluginListPanel();
        StellwerkSimPluginManager.EVENT_BUS.register(pluginListPanel);
        copyrightPanel = new CopyrightPanel();
        StellwerkSimPluginManager.EVENT_BUS.register(copyrightPanel);

        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BorderLayout(3, 3));
        contentPane.add(pluginListPanel, BorderLayout.WEST);
        contentPane.add(pluginInfoPanel, BorderLayout.CENTER);
        contentPane.add(copyrightPanel, BorderLayout.SOUTH);
        contentPane.setEnabled(false);
    }
    
    @Subscribe
    public void preInit(PreInitializationEvent event) {
        ProgressEvent pe = new ProgressEvent();
        pe.progress = -1;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
    }
    
    @Subscribe
    public void postInit(PostInitializationEvent event) {
        ProgressEvent pe = new ProgressEvent();
        pe.done = true;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
    }
    
    @Subscribe
    public void preShutdown(PreShutdownEvent event) {
        this.setEnabled(false);
        ProgressEvent pe = new ProgressEvent();
        pe.progress = -1;
        StellwerkSimPluginManager.EVENT_BUS.post(pe);
    }

    public static StellwerkSimPluginManagerJFrame getInstance() {
        if (instance == null) {
            try {
                instance = new StellwerkSimPluginManagerJFrame();
            } catch (HeadlessException | FontFormatException | IOException ex) {
                Logger.getLogger(StellwerkSimPluginManagerJFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return instance;
    }
}
