package de.sts_plugins.manager.gui;

import de.sts_plugins.manager.plugin.ManagedObjectFilterRegistry;
import de.sts_plugins.manager.plugin.filter.event.FilterObjectChanged;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author BR 89
 */
public class PluginFilterComboBoxModel implements MutableComboBoxModel {

    private Object selected;

    @Override
    public void addElement(Object item) {
        ManagedObjectFilterRegistry.getInstance().addElement(item);
    }

    @Override
    public void removeElement(Object obj) {
        ManagedObjectFilterRegistry.getInstance().removeElement(obj);
    }

    @Override
    public void insertElementAt(Object item, int index) {
        ManagedObjectFilterRegistry.getInstance().insertElementAt(item, index);
    }

    @Override
    public void removeElementAt(int index) {
        ManagedObjectFilterRegistry.getInstance().removeElementAt(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selected = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    @Override
    public int getSize() {
        return ManagedObjectFilterRegistry.getInstance().getSize();
    }

    @Override
    public Object getElementAt(int index) {
        return ManagedObjectFilterRegistry.getInstance().getElementAt(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        ManagedObjectFilterRegistry.getInstance().addListDataListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        ManagedObjectFilterRegistry.getInstance().removeListDataListener(l);
    }

}
