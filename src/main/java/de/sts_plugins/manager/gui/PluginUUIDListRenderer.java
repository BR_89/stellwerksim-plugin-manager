package de.sts_plugins.manager.gui;

import de.sts_plugins.manager.gui.font.FontAwesomeUtils;
import de.sts_plugins.manager.plugin.PluginManager;
import java.awt.Component;
import java.util.UUID;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author BR 89
 */
public class PluginUUIDListRenderer implements ListCellRenderer {

    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        UUID plugin = (UUID) value;
        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        String icon = FontAwesomeUtils.Icon.DOWNLOAD.toHtml();
        PluginManager pm = PluginManager.getInstance();
        if (pm.isInstalled(plugin)) {
            if (pm.hasUpdate(plugin)) {
                icon = FontAwesomeUtils.Icon.REPEAT.toHtml();
            } else {
                icon = FontAwesomeUtils.Icon.DESKTOP.toHtml();
            }
        }
        String prefix = icon + " ";
        String suffix = "";
        if (pm.isFavorite(plugin)) {
            suffix = " " + FontAwesomeUtils.Icon.STAR.toHtml();
        }
        renderer.setText("<html>" + prefix + pm.getName(plugin) + suffix + "</html>");
        return renderer;
    }

}
