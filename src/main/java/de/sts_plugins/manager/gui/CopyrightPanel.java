package de.sts_plugins.manager.gui;

import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.gui.event.ProgressEvent;
import de.sts_plugins.manager.gui.event.ProgressFinishedEvent;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 *
 * @author BR 89
 */
public class CopyrightPanel extends javax.swing.JPanel {
    
    protected JProgressBar progressBarComponent;
    protected JLabel copyrightComponent;
    
    public CopyrightPanel() {
        setLayout(new BorderLayout());
        progressBarComponent = new JProgressBar();
        progressBarComponent.setMinimum(0);
        copyrightComponent = new JLabel("StellwerkSim-Plugin-Manager" + " © BR 89", JLabel.CENTER);
        this.add(progressBarComponent, BorderLayout.NORTH);
        this.add(copyrightComponent, BorderLayout.CENTER);
    }
    
    @Subscribe
    public void progress(ProgressEvent event) {
        progressBarComponent.setIndeterminate(event.progress < 0);
        if (event.progress >= 0) {
            progressBarComponent.setValue(event.progress);
        }
        if (event.max > 0) {
            progressBarComponent.setMaximum(event.max);
        }
        if (event.done) {
            StellwerkSimPluginManager.EVENT_BUS.post(new ProgressFinishedEvent());
        }
    }
    
    @Subscribe
    public void progressFinished(ProgressFinishedEvent event) {
        progressBarComponent.setValue(0);
        progressBarComponent.setMaximum(0);
        progressBarComponent.setIndeterminate(false);
    }
}
