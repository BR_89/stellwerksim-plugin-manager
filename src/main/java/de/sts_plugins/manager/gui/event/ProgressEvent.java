package de.sts_plugins.manager.gui.event;

/**
 *
 * @author BR 89
 */
public class ProgressEvent {

    public boolean done;
    public int progress;
    public int max;

    public ProgressEvent() {
        done = false;
        progress = 0;
        max = 0;
    }

}
