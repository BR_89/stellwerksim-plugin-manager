package de.sts_plugins.manager.gui;

import de.sts_plugins.manager.plugin.PluginUUIDList;
import com.google.common.eventbus.Subscribe;
import de.sts_plugins.manager.plugin.ManagedObjectFilterRegistry;
import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.init.PostInitializationEvent;
import de.sts_plugins.manager.event.shutdown.PreShutdownEvent;
import de.sts_plugins.manager.gui.font.FontAwesomeUtils;
import de.sts_plugins.manager.plugin.event.PluginFilterChangedEvent;
import de.sts_plugins.manager.plugin.event.PluginListChangedEvent;
import de.sts_plugins.manager.plugin.event.PluginSelectedEvent;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.UUID;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;

/**
 *
 * @author BR 89
 */
public class PluginListPanel extends JPanel {

    protected JScrollPane scrollComponent;
    protected JList listComponent;
    protected PluginUUIDList listModel;
    protected JPanel topComponent;
    protected JComboBox filterComponent;
    protected JButton updateComponent;
    protected PluginFilterComboBoxModel filterComponentModel;

    public PluginListPanel() {
        this.init();
    }
    
    private void init() {
        this.setLayout(new BorderLayout(1, 1));
        this.listModel = new PluginUUIDList();
        StellwerkSimPluginManager.EVENT_BUS.register(listModel);
        this.listComponent = new JList();
        this.listComponent.setModel(listModel);
        this.listComponent.setEnabled(false);
        this.listComponent.setCellRenderer(new PluginUUIDListRenderer());
        this.listComponent.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.listComponent.addListSelectionListener((ListSelectionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent((UUID) listComponent.getSelectedValue()));
        });
        this.scrollComponent = new JScrollPane(listComponent, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        this.topComponent = new JPanel(new BorderLayout(1, 1));
        this.updateComponent = new JButton("<html>" + FontAwesomeUtils.Icon.REFRESH.toHtml() + "</html>");
        this.updateComponent.setBorder(BorderFactory.createEmptyBorder());
        this.updateComponent.setContentAreaFilled(false);
        this.updateComponent.addActionListener((ActionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new PluginFilterChangedEvent((String) filterComponent.getSelectedItem().toString()));
        });
        this.filterComponentModel = new PluginFilterComboBoxModel();
        this.filterComponent = new JComboBox();
        this.filterComponent.setEnabled(false);
        this.filterComponent.setModel(this.filterComponentModel);
        this.filterComponent.setFont(this.filterComponent.getFont().deriveFont(FontAwesomeUtils.getFont().getSize()));
        this.filterComponent.setEditable(false);
        this.filterComponent.addActionListener((ActionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new PluginFilterChangedEvent((String) filterComponent.getSelectedItem().toString()));
        });
        this.topComponent.add(this.updateComponent, BorderLayout.EAST);
        this.topComponent.add(this.filterComponent, BorderLayout.CENTER);

        this.add(topComponent, BorderLayout.NORTH);
        this.add(scrollComponent, BorderLayout.CENTER);
    }

    @Subscribe
    public void postInitialization(PostInitializationEvent event) {
        this.filterComponent.setEnabled(true);
        this.filterComponent.setSelectedItem(ManagedObjectFilterRegistry.getInstance().getFilter("Alle"));
        this.listComponent.setEnabled(true);
    }
    
    @Subscribe
    public void pluginListChanged(PluginListChangedEvent event) {
        if (this.listModel.getSize() > 0) {
            this.listComponent.setSelectedIndex(0);
        } else {
            StellwerkSimPluginManager.EVENT_BUS.post(new PluginSelectedEvent(null));
        }
    }
    
    @Subscribe
    public void pluginSelected(PluginSelectedEvent event) {
        this.listComponent.updateUI();
    }
    
    @Subscribe
    public void preShutdown(PreShutdownEvent event) {
        this.listComponent.setEnabled(false);
        this.filterComponent.setEnabled(false);
    }
}
