package de.sts_plugins.manager.gui;

import de.sts_plugins.manager.StellwerkSimPluginManager;
import de.sts_plugins.manager.event.OpenUrlEvent;
import de.sts_plugins.manager.event.shutdown.ShutdownEvent;
import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author BR 89
 */
public final class TrayMenu extends PopupMenu {

    static TrayMenu instance;
    private final TrayIcon trayIcon = new TrayIcon(new ImageIcon(getClass().getResource("/images/tray.png")).getImage());
    private final MenuItem home = new MenuItem() {
    };
    private final MenuItem kill = new MenuItem() {
    };

    private TrayMenu() {
        trayIcon.setPopupMenu(this);
        trayIcon.setToolTip("Stellwerk-Sim-Plugin-Manager");
        trayIcon.setImageAutoSize(true);
        home.setLabel("Webseite");
        home.addActionListener((ActionEvent e) -> {
            try {
                StellwerkSimPluginManager.EVENT_BUS.post(new OpenUrlEvent(new URL("https://bitbucket.org/BR_89/stellwerksim-plugin-manager/overview")));
            } catch (MalformedURLException ex) {
                Logger.getLogger(TrayMenu.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        kill.setLabel("Beenden");
        kill.addActionListener((ActionEvent e) -> {
            StellwerkSimPluginManager.EVENT_BUS.post(new ShutdownEvent());
        });
        add(home);
        add(kill);
    }

    public static TrayMenu getInstance() {
        if (instance == null) {
            instance = new TrayMenu();
        }
        return instance;
    }

    public final void add() {
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            try {
                tray.add(this.trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(TrayMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
