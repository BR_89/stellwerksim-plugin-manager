package de.sts_plugins.api;

import de.sts_plugins.api.plugin.IManagedObjectFilter;
import de.sts_plugins.api.plugin.IManagedObject;
import de.sts_plugins.manager.plugin.ManagedObjectFilterRegistry;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BR 89
 */
public class ManagedObjectFilterRegistryTest {

    @Test
    public void testHasFilter() {
        System.out.println("hasFilter");
        String name = "filter";
        ManagedObjectFilterRegistry instance = new ManagedObjectFilterRegistry();
        assertFalse(instance.hasFilter(name));
        instance.addElement(new IManagedObjectFilterImpl(name));
        assertTrue(instance.hasFilter(name));
    }

    @Test
    public void testGetFilter() {
        System.out.println("getFilter");
        String name = "filter";
        ManagedObjectFilterRegistry instance = new ManagedObjectFilterRegistry();
        assertEquals(null, instance.getFilter(name));
        IManagedObjectFilter filter = new IManagedObjectFilterImpl(name);
        instance.addElement(filter);
        assertEquals(filter, instance.getFilter(name));
    }

    class IManagedObjectFilterImpl implements IManagedObjectFilter {

        protected String name;

        public IManagedObjectFilterImpl(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public List<IManagedObject> getMatchingElements(List<IManagedObject> input) {
            return input;
        }

    }

}
