package de.sts_plugins.api;

import de.sts_plugins.api.plugin.Requirement;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BR 89
 */
public class DependencyTest {
    

    @Test
    public void testGetUuid() {
        System.out.println("getUuid");
        UUID expResult = UUID.randomUUID();
        Requirement instance = new Requirement(expResult, "1.0");
        UUID result = instance.getUuid();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetVersion() {
        System.out.println("getVersion");
        String expResult = "1.0";
        Requirement instance = new Requirement(UUID.randomUUID(), expResult);
        String result = instance.getVersion();
        assertEquals(expResult, result);
    }
    
}
