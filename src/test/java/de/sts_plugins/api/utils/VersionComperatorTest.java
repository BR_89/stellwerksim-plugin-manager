package de.sts_plugins.api.utils;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BR 89
 */
public class VersionComperatorTest {

    @Test
    public void testCompareVersionStringsEqual() {
        System.out.println("compareVersionStringsEqual");
        // 1.0 == 1.0
        String a = "1.0";
        String b = "1.0";
        int expResult = 0;
        int result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // null == null
        a = null;
        b = null;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);
    }

    @Test
    public void testCompareVersionStringsA() {
        System.out.println("compareVersionStringsA");
        // 1.1 > 1.0
        String a = "1.1";
        String b = "1.0";
        int expResult = 1;
        int result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.1 > null
        b = null;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);
    }

    @Test
    public void testCompareVersionStringsB() {
        System.out.println("compareVersionStringsB");
        // 1.0 < 1.1
        String a = "1.0";
        String b = "1.1";
        int expResult = -1;
        int result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // null < 1.1
        a = null;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);
    }

    @Test
    public void testCompareVersionStringsDifferentLength() {
        System.out.println("compareVersionStringsDifferentLength");
        // 1.0.1 < 1.1
        String a = "1.0.1";
        String b = "1.1";
        int expResult = -1;
        int result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.2.1 > 1.1
        a = "1.2.1";
        expResult = 1;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.2.1 < 1.2.1.0
        b = "1.2.1.0";
        expResult = -1;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);
    }

    @Test
    public void testCompareVersionStringsSuffix() {
        System.out.println("compareVersionStringsSuffix");
        // 1.0-alpha < 1.0-beta
        String a = "1.0-alpha";
        String b = "1.0-beta";
        int expResult = -1;
        int result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.0-rc1 > 1.0-beta
        a = "1.0-rc1";
        expResult = 1;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.0-rc1 < 1.0-rc2
        b = "1.0-rc2";
        expResult = -1;
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);

        // 1.0-rc1 < 1.0
        b = "1.0";
        result = VersionComperator.compareVersionStrings(a, b);
        assertEquals(expResult, result);
    }

    @Test
    public void testSplitInvervall() {
        System.out.println("splitInvervall");
        // "[0.9;0.9.2)" -> [ "[0.9;0.9.2)" ]
        String input = "[0.9;0.9.2)";
        String[] expResult = new String[]{"[0.9;0.9.2)"};
        String[] result = VersionComperator.splitInvervall(input);
        assertArrayEquals(expResult, result);

        // "[0.9;0.9.2),[9.2;9.3]" -> [ "[0.9;0.9.2)", "[9.2;9.3]" ]
        input = "[0.9;0.9.2),[9.2;9.3]";
        expResult = new String[]{"[0.9;0.9.2)", "[9.2;9.3]"};
        result = VersionComperator.splitInvervall(input);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testVersionInIntervallClosed() {
        System.out.println("versionInIntervallClosed");

        String version = "1.0";
        String intervall = "[0;0.9]";
        boolean expResult = false;
        boolean result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[0;1.0]";
        expResult = true;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[0;2.0]";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[1.0;2.0]";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[1.1;2.0]";
        expResult = false;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);
    }

    @Test
    public void testVersionInIntervallOpen() {
        System.out.println("versionInIntervallOpen");

        String version = "1.0";
        String intervall = "(0;0.9)";
        boolean expResult = false;
        boolean result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(0;1.0)";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(0;2.0)";
        expResult = true;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(1.0;2.0)";
        expResult = false;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(1.1;2.0)";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);
    }

    @Test
    public void testVersionInIntervallMixed() {
        System.out.println("versionInIntervallMixed");

        String version = "1.0";
        String intervall = "(0;0.9]";
        boolean expResult = false;
        boolean result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(0;1.0]";
        expResult = true;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[0;2.0)";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "[1.0;2.0)";
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);

        intervall = "(1.0;2.0]";
        expResult = false;
        result = VersionComperator.versionInIntervall(version, intervall);
        assertEquals(expResult, result);
    }

    @Test
    public void testVersionInIntervalls() {
        System.out.println("versionInIntervalls");

        String version = "1.0";
        String intervall = "(0;0.9],(1.0;1.2.3]";
        boolean expResult = false;
        boolean result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
        
        intervall = "(0;0.9],[1.0;1.2.3]";
        expResult = true;
        result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
        
        
        version = "1.2";
        result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
        
        version = "1.2.3";
        result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
        
        intervall = "(0;0.9],[1.0;1.2.3)";
        expResult = false;
        result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
    }

    @Test
    public void testVersionInIntervallsSingleVersion() {
        System.out.println("versionInIntervallsSingleVersion");

        String version = "1.0";
        String intervall = "0.9";
        boolean expResult = false;
        boolean result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
        
        intervall = "1.0";
        expResult = true;
        result = VersionComperator.versionInIntervalls(version, intervall);
        assertEquals(expResult, result);
    }
    
}
