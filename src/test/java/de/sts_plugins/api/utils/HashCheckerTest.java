package de.sts_plugins.api.utils;

import java.io.File;
import java.nio.file.Files;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BR 89
 */
public class HashCheckerTest {

    @Test
    public void testCalculateByteHash() throws Exception {
        System.out.println("calculateByteHash");
        byte[] bytes = { 15, 15, 31, 31, 0, 0, 21, 42, 0 };

        String expResult = "4d16943f4f1e00eea3292596b1eb3083";
        String result = HashChecker.calculateByteHash(bytes, HashChecker.Type.MD5);
        assertEquals(expResult, result);
    }

    @Test
    public void testCalculateFileHash() throws Exception {
        System.out.println("calculateFileHash");
        byte[] bytes = { 15, 15, 31, 31, 0, 0, 21, 42, 0 };
        File resource = File.createTempFile("test", "asBytes");
        resource.deleteOnExit();
        Files.write(resource.toPath(), bytes);

        String type = "md5";
        String expResult = "4d16943f4f1e00eea3292596b1eb3083";
        String result = HashChecker.calculateFileHash(resource, HashChecker.Type.MD5);
        assertEquals(expResult, result);
    }

    @Test
    public void testCalculateTextHash() throws Exception {
        System.out.println("calculateTextHash");
        String text = "";
        String type = "md5";
        String expResult = "d41d8cd98f00b204e9800998ecf8427e";
        String result = HashChecker.calculateTextHash(text, HashChecker.Type.MD5);
        assertEquals(expResult, result);
        type = "SHA-256";
        expResult = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
        result = HashChecker.calculateTextHash(text, HashChecker.Type.SHA256);
        assertEquals(expResult, result);
    }
    
}
