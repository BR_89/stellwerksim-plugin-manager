package de.sts_plugins.api.utils;

import java.util.Base64;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BR 89
 */
public class SignatureCheckerTest {

    @Test
    public void testGetByteDataFromString() {
        System.out.println("getByteDataFromString");
        byte[] expResult = { 15, 15, 31, 31, 0, 0, 21, 42, 0 };
        String string = SignatureChecker.START_TOKEN + Base64.getEncoder().encodeToString(expResult) + SignatureChecker.MIDDLE_TOKEN;
        byte[] result = SignatureChecker.getByteDataFromString(string);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testGetByteSignatureFromString() {
        System.out.println("getByteSignatureFromString");
        byte[] expResult = { 15, 15, 31, 31, 0, 0, 21, 42, 0 };
        String string = SignatureChecker.MIDDLE_TOKEN + Base64.getEncoder().encodeToString(expResult) + SignatureChecker.END_TOKEN;
        byte[] result = SignatureChecker.getByteSignatureFromString(string);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testGetStringDataFromString() throws Exception {
        System.out.println("getStringDataFromString");
        String expResult = "Test succeded.";
        String string = SignatureChecker.START_TOKEN + Base64.getEncoder().encodeToString(expResult.getBytes("UTF-8")) + SignatureChecker.MIDDLE_TOKEN;
        String result = SignatureChecker.getStringDataFromString(string);
        assertEquals(expResult, result);
    }
    
}
